\documentclass[10pt,authoryear,multilingual,german]{elsarticle}
\let\dateenglish\dateaustralian

\usepackage{etex}

\usepackage{xcolor, soul}


\usepackage{graphics}
\usepackage{amsmath,amsfonts,amssymb,epsfig}
\usepackage{url}
\usepackage{german}
\usepackage{ifthen}
\usepackage{fullpage}
\usepackage{lscape}

% Nur fuer Randbenmerkungen
\usepackage{marginnote}
\usepackage[outer=6cm,marginparwidth=5.5cm]{geometry}

\newcommand{\TODO}[1]{\marginnote{\begin{scriptsize} #1 \end{scriptsize}}}

\usepackage{tilde}

\newif\ifrefsbib
\refsbibfalse
% To toggle, uncomment the following line
\refsbibtrue

\usepackage{natbib}
\ifrefsbib
%\bibliographystyle{elsarticle-num-names}
\bibliographystyle{cc2}
\fi

\usepackage{booktabs}
\usepackage{caption}
\usepackage{threeparttable}

\usepackage[latin1]{inputenc}

\newcommand{\Ff}{\mathbb{F}}
\newcommand{\Ee}{\mathbb{E}}
\newcommand{\Nn}{\mathbb{N}}
\newcommand{\Zz}{\mathbb{Z}}
\newcommand{\eop}{\hspace*{\fill}$\Box$}
\newcommand{\eqdef}{\overset{\mathrm{def}}{=\joinrel=}}

\newcommand{\ie}{i.e.~}
\newcommand{\eg}{e.g.~}

\newif\ifanonymous%
\anonymoustrue%
\anonymousfalse%
 
\DeclareMathOperator{\parity}{parity} 

\begin{document}

\begin{frontmatter}

\title{Comparative analysis of random generators}

\begin{abstract}
    We report on experiments with selected software and hardware
    (pseudo)random generators under controlled conditions and compare their throughput and consumed entropy. 
    In software, we distinguish between number theoretic generators (which come with corresponding security reductions) and generators based on AES. 
    Physical random generators covered by our study are the hardware generator PRG310-4 and the generators \texttt{/dev/random} and \texttt{/dev/urandom} as implemented in the Linux kernel.
\end{abstract}

\begin{keyword}
random generator \sep pseudorandom \sep  cryptography \sep Linux \sep /dev/random \sep entropy
\end{keyword}

\author[fkie]{Johannes vom Dorp}
\ead{johannes.vom.dorp@fkie.fraunhofer.de}

\author[bit]{Joachim von zur Gathen} 
\ead{gathen@bit.uni-bonn.de}

\author[genua]{Daniel Loebenberger}
\ead{daniel\_loebenberger@genua.de}

\author[bit]{Jan~L\"{u}hr} 
\ead{luehr@cs.uni-bonn.de}

\author[bit]{Simon Schneider} 
\ead{schneid@cs.uni-bonn.de}

\address[bit]{Bonn-Aachen International Center for Information Technology (B-IT),\\Universit\"at Bonn, Germany}
% \address[unibonn]{University of Bonn}
\address[genua]{genua GmbH, Kirchheim bei M\"{u}nchen}
\address[fkie]{Fraunhofer FKIE, Bonn, Germany}

\end{frontmatter}

\selectlanguage{english}

\section{Introduction}\label{s:int}


Randomness is an indispensable tool in computer algebra.  Even for the basic
and apparently simple task of factoring univariate polynomials over finite
fields the only known efficient (= polynomial-time) algorithms are
probabilistic, and finding a deterministic solution is the central
theoretical problem in that area. For many, but not all, tasks of
computational linear algebra the most efficient algorithms today use pre-
and post-multiplication by random matrices, as introduced in \cite{borgat82}
and refined in many ways since then; it is now a staple tool in that field.

Even greater is the importance in cryptography, say for generating all kinds
of secret keys.  Deterministic or predictable keys would allow an adversary
to reproduce them and break the cryptosystem. Since the random keys are only
known to the legitimate user, a brute-force attack would require an
exhaustive search of a key space that is prohibitively large, thus
preventing a feasible or practical search.

Now a fundamental problem is that we treat our computers as deterministic entities
that, by their nature, cannot generate randomness.
This is not literally true because tiny random influences may come from effects like cosmic
radiation, but these are easily controlled by error-correcting measures.
Furthermore, quantum computers provide randomness naturally.
Even more, they can factor integers in polynomial time and break most of the classical
cryptosystems, say RSA, due to the famous algorithm by \cite{sho99a}.
But it is a matter of opinion whether or when scalable quantum computing will become a reality.

How can we deal with this basic impossibility to generate randomness on our computers?
After all, we do want secure internet connections and much more.
A common solution works in two steps:
\begin{enumerate}
\item
Produce values that are supposed to carry a reasonable amount of randomness,
using an outside source, say measuring some physical process that looks chaotic to us.
\item
Extend a small amount of true randomness to an arbitrarily large amount of \emph{pseudorandomness}.
\end{enumerate}

And what does that mean?
True randomness refers to \emph{uniform randomness}.
A uniformly random source with values in a finite set produces each element of the set
with the same probability.
A pseudorandom source, usually called a \emph{pseudorandom generator},
produces values that cannot be distinguished efficiently from uniformly random ones.
That is, no efficient (polynomial-time) machine, deterministic or probabilistic, exists
which can ask for an arbitrarily long stream of values, is given either a uniformly random
stream or a stream generated by the pseudorandom generator, and then decides
(with non-negligible probability of correctness) which of the two is the case.

Given a generator claimed to be pseudorandom, how do you prove that no such distinguishing machine exists?
Unfortunately, we cannot. This is embodied in the question $P \neq NP$ posed by
\cite{coo71} and, almost half a century later, is still an open
one-million-dollar \emph{millennium problem}.
But computational complexity offers a solution: reductions.
We take some algorithmic problem which is considered to be hard (not solvable in (random) polynomial time)
and show that the existence of an efficient distinguisher implies a solution to the problem.
A well-known such problem is the factorization of large integers.
Many researchers have looked at it and no solution is known (except on the as yet hypothetical quantum computers).
Such a reduction is currently the best way of establishing pseudorandomness.

Probability theory suggest a different approach: measure the entropy. It expresses the ``amount'' of randomness that a source produces.
Unfortunately, entropy cannot be measured practically (\cite{golsah99}, \cite{gatloe17}).
As a way out, sometimes the \emph{block entropy} is measured, see below.
It will show large statistical abnormalities, if present, within the output stream,
but cannot indicate their absence.
In our context, this is rather useless, since even cryptographically weak generators
may possess high block entropy.

An intermediate step before seeding a pseudorandom generator from a source is \emph{randomness extraction}.
Some of the methods in that area only require a lower bound on the source's \emph{min-entropy},
a more intuitive measure for randomness.
By their nature, physical random generators are not amenable to mathematically rigorous proofs of such bounds.
Quite justifiably, reasonable engineering standards ignore such theoretical stipulations in practice,
but we give some weight to them.

For physical hardware generators, applying a series of statistical tests like the above seems to be the only approach, and we also use it for lack of alternatives.
For instance, lack of sufficient entropy caused severe weakening of Debian's OpenSSL implementation, see \cite{sch08g}. 
However, experts know the dangers of this approach quite well:

\begin{quotation}
``The main part of a security evaluation considers the generic design
and its implementation. The central goal is to quantify (at least a
lower bound for) the entropy per random bit. Unfortunately, entropy
cannot be measured as voltage or temperature. Instead, entropy is a
property of random variables and not of observed realizations (here:
random numbers).'' \citep{kilsch08}
\end{quotation}

This warning is often ignored in the literature.

Pseudorandom generators come in two flavors: based on a symmetric
cryptosystem like the Advanced Encryption Standard (AES),
or based on number-theoretic hard problems such as factoring integers.
The general wisdom is that the latter are much slower than the former.
The main goal of this paper is to examine this opinion which, to our
surprise, turns out to be untenable.

We study one hardware generator; by its nature, it is out of scope for theoretical comparisons.
Among the software pseudorandom generators, AES and some of the number-theoretic
ones perform roughly equally well, provided they are run with \emph{fair} implementations.
We use corresponding home-brew code to run them, implemented with the same care.
However, if the AES generator is run on specialized \emph{AES-friendly} hardware,
it outperforms the others by a large distance. This comes as no surprise.

Our comparative analysis covers some popular pseudorandom generators and two physical sources of randomness. 
Of course, the choice of possible generators is vast. We thus try to select examples of the respective classes to get a representative picture of the whole situation. 
Our measurements were reported in \cite{burdor15},
so that their absolute values are somewhat outdated.
But that is not the point here: we strive for a fair comparison of the generators,
and that can be expected to carry through to later hardware versions, with a grain of salt.

An example of the insatiable thirst for randomness are TLS transactions, which consume at 43.000 new transactions per second (cipher suite ECDHE-ECDSA-AES256-GCM-SHA384) on a single Intel Xeon based system (cf. \cite{nginx} product information) 1376 KB/s of randomness to generate pre-master secrets of 256 bits each---ideally, using only negligible CPU resources.

% Thus, modern computer systems desperately need both, cryptographically secure pseudorandom generators with high performance and a physical entropy source which delivers seed entropy at acceptable speed. 
%The view that pseudorandom generators are to be considered as separate cryptographic primitives is by far not new, see \cite{kelsch98}.

In our setup we use as a source of random seeds one particular output of the hardware generator PRG310-4, which was analyzed in \cite{schkil03}. 
On the software side we discuss several number-theoretic generators, namely the 
%(in general non-cryptographic) 
linear congruential generator, the RSA generator, and the Blum-Blum-Shub generator, all at carefully selected truncation rates of the output.
The generators come with certain security reductions. 
For comparison we add to our analysis pseudorandom generators based on a well-studied block cipher, in our case AES in counter mode.

The article is structured as follows: We first present previous work on generator analysis in Section \ref{s:rw} before giving a detailed overview of the generators in Section \ref{s:gen}.
The main contribution is the evaluation regarding throughput and entropy consumption in Section \ref{s:eval}. We conclude and elaborate on future work in Section \ref{s:fw}.
% There is one Appendix, which contains a mathematical deduction on how many runtime experiments we have to run until we 
% can expect that our results reflect the true behavior close enough with high probability.

All algorithms except the one employing AES-NI were implemented in a textbook manner using non-optimized C-code, thus providing a fair comparison. 
The source code of the algorithms is available at \cite{burdor15a}.
% JvzG: n�chster Satz ok?
%This paper is the result of a class project in a course taught by Joachim von zur Gathen and Daniel Loebenberger.

\section{Related work}\label{s:rw}

Concerning physical generators, \cite{schkil03} analyze noisy diodes as a random source, providing a model for its entropy. 
One example of a noisy diodes based generator is the generator PRG310-4, which is distributed by \cite{ber14}. 
Linux' VirtIO generator as used in  \texttt{/dev/random} is illustrated by \cite{gutpin06} and explained by  \cite{lac12}. Combined, they provide a clear picture of its inner workings. 
Additionally, there is the study by \cite{bsi-linux-rng17} in which the quality of \texttt{/dev/random} and \texttt{/dev/urandom} is studied with respect to the functionality classes for random generators as given by \cite{bsi11}.

Referring to pseudorandom generators, the RSA based generator is explained
in \cite{sha83b}, \cite{fissch00}, and \cite{stepie06}. Its cryptographic
security is shown in \cite{alecho88b} and extended in \cite{stepie06}.
Linear congruential generators were first proposed by \cite{leh51}. Attacks
were discussed in \cite{boy89} and \cite{has85}. They all exploited its
simple linear structure and come with a specific parameterization. Not all
parameterizations --- such as truncating its output to a single bit --- have
been attacked successfully as of today. \cite{conshp05} analyze this in
depth concluding that (for some cases)
\begin{quotation}
``[\dots] we do not know if the truncated linear congruential generator can still be cryptanalyzed.''
\end{quotation}
\cite{blublu86} introduced the Blum-Blum-Shub generator. \cite{alecho88b} and \cite{fissch00} show that the integer modulus can be factored, given a  
distinguisher for the generator. 

A totally different approach for the construction of pseudorandom generators are the ones based on established cryptographic primitives. \cite{nist12-800-90A} specifies several standards for producing cryptographically secure random numbers. Besides hash-based techniques, there is also a standard employing a block cipher in counter mode, see also \cite{nist01-800-38A} for this purpose. 

RFC 4086, see \cite{RFC4086}, compares different techniques and provides a de-facto standard focussing on internet engineering. 
There, several entropy pool techniques and randomness generation procedures are specified.
However, RFC 4086 lacks recommendations for the ciphers to be used in OFB and CTR generation.
We show here that such a general recommendation would also be ill-suited since the optimal choice depends heavily on the platform used.

We are not aware of any comprehensive, fair benchmarking survey for all the generators mentioned above that integrates them into the Linux operating system.

\section{The generators}\label{s:gen}
In the following, each generator which was implemented or applied for the comparative analysis is briefly presented. 
The output of a pseudorandom generator is, by definition, not efficiently distinguishable from uniform randomness, see for example \cite{gol01}.
When assuming that certain problems in algorithmic number theory (such as 
%the difficulty of 
factoring integers) are difficult to solve, the Blum-Blum-Shub, and RSA generators with suitable truncation have this property, but the linear congruential generator does not. 
Also the AES-based generator does not, but assuming AES to be a secure cipher, the AES-based generator is pseudorandom as well.

% As the two generators in question have been exposed to be predictable, this definition does not apply to them.

\subsection{Linux \texttt{/dev/random} and \texttt{/dev/urandom}}\label{gen:dev}
%VirtIO}
The German Federal Office for Information Security\footnote{Bundesamt f\"{u}r Sicherheit in der Informationstechnik (BSI)}  sets cryptographic standards in Germany and judges \texttt{/dev/random} as a non-physical true random number generator (i.e., an NTG.1 generator in the terminology of \cite{bsi11}) for most Linux kernel versions, see \cite{bsi17}.

\texttt{/dev/urandom}, however, does not fulfill the requirements for the class NTG.1, since property NTG.1.1 requires:
\begin{quotation}
``The RNG shall test the external input data provided by a non-physical entropy source in order to estimate the entropy and to detect non-tolerable statistical defects [\dots]'', see \cite{bsi11}.
\end{quotation}
This is clearly not met by \texttt{/dev/urandom} due to the fact that it is non-blocking. Indeed, \texttt{/dev/urandom} fulfills all other requirements of the class NTG.1 (except NTG.1.1 as stated). In particular, it is a DRG.3 generator.

As already mentioned, system events are used to gather entropy on Linux Systems. 
These events are post-processed and made available to the devices \texttt{/dev/random} and \texttt{/dev/urandom}. This includes estimating the entropy of the event and mixing.

\texttt{/dev/urandom}, however will still supply the user with ``randomness'' without checking whether the entropy-pool is still sufficiently filled. In fact, the user is instead supplied with pseudorandom data in favor of speed requirements. 

On the OpenBSD operating system, none of the random devices is implemented in a blocking mode.
The idea is that much potentially bad randomness is still better than the parsimonious use of high-quality randomness. 
This is in contrast to the opinion, as for example held by the BSI, that one should require all used randomness to be of guaranteed good quality.
Up to now, there is still no consensus on this issue.

Since the \texttt{/dev/urandom} device has undergone a major change introduced by \cite{ker16} in kernel version 4.8., two kernel versions were benchmarked to test the differences. Namely the original Ubuntu 16.04.\ kernel 4.4.0 and the more recent version 4.10.0.

\subsection{PRG310-4}
The PRG310-4 gathers entropy from a system of two noisy diodes, see \cite{ber14}, and is connected
to a computer via USB. Similar variants exist for different interface types. According to  \cite{ber14}, its behavior follows the stochastic model in  \cite{schkil03}, who show that 
 \begin{quotation}
   ``[...] principally, this RG [design] could generate up to $700000$ random bits per second with an average entropy
$1-10^{-5}$.''
 \end{quotation}
\cite{ber14} mentions that this device satisfies all requirements for class PTG.3, which are ``hybrid physical random number generator with cryptographic post-processing'' in the terminology of \cite{bsi11}. 

\subsection{AES in counter mode}
Due to the fact that since 2008 there is AES-NI\footnote{For a white paper of AES-NI, see \cite{gue10}}, realizing dedicated processor instructions on Intel and AMD processors for the block cipher AES as standardized by \cite{fips197}, we add to our comparison the AES counter mode generator. This generator is also standardized by \cite{nist12-800-90A} and produces a sequence of $128$ bit blocks. We aim at security level of $128$ bits, thus employing AES-128 as the underlying block cipher.

The security of the AES generator directly reduces to the security of AES. Indeed, any distinguisher for the pseudorandom generator gives an equally good distinguisher for AES in counter mode. Assuming the latter to be secure, one concludes that also the pseudorandom generator is secure. 

However, in contrast to the number-theoretic generators described below, we do not have any reductionist argument in our hands to actually prove that the generator is secure if some presumably hard mathematical problem is intractable. We need to trust that the cipher AES is secure---and the dedicated processor instructions on our CPU work as specified.

AES in counter mode is an DRG.3 generator in the terminology of \cite{bsi11}. 

\subsection{Linear Congruential generators}
\label{sec:lcg}

The linear congruential generator as presented in \cite{leh51} produces for
$i \geq 1$ a sequence of values in $x_i \in \mathbb{Z}_M$, generated by
applying for $a,b \in \mathbb{Z}_M$ iteratively
\begin{equation*}
  x_i = a \cdot x_{i-1} + b  \text{ in $\mathbb{Z}_M$}
\end{equation*}
to a secret random seed $x_0 \in \mathbb{Z}_M$ provided by an external source. The parameters $a$, $b$, and $M$ are also kept secret and chosen from the external source.

While the byte distribution of linear congruential generator outputs is generally well-distributed, with byte entropy close to maximal, the generated sequences are predictable and therefore cryptographically insecure. 
%A paper on predicting pseudorandom sequences in general 

The author of the two articles \cite{plu82b} and \cite{boy89} describes how to recover the secrets $a$, $b$ and $M$ from the sequence of $(x_i)_{i \geq 0}$ alone.
A possible mitigation against this attack is to output only some \emph{least significant bits} of the $x_i$.
% \citet{has85} describes a lattice based attack on truncated LCGs where $(s,t,M)$ are public.
\cite{has85} describe a lattice based attack on truncated linear congruential generator where all parameters are public. \cite{ste87} showed that also in the case when the parameters are kept secret.
This attack can be used to predict linear congruential generators that output at least $\frac{1}{3}$ of the bits of the $x_i$. \cite{conshp05} write that there is no cryptanalytic attack known when only approximately $k = ~log_2 ~log_2 M$ bits are output per round.

We are neither aware of a more powerful attack on the linear congruential generator nor of a more up-to-date security argument for truncated linear congruential generators.

In our evaluation we used a prime modulus $M$ with 2048 bit, which corresponds to a security level of $128$ bits according to the \cite{bsi17-tr02102} guideline TR-02102-1. Per round we output $k = 11$ bits, which coincides with the value from \cite{conshp05} mentioned above. For comparison, we also run the generator with modulus $M = 2^{2048}$ and full output, that is, no truncation, which is basically the fastest number-theoretic generator we can hope for.

The full linear congruential generator is not a pseudorandom generator in the terminology of \cite{bsi11}, since it does not provide forward secrecy. If the sketched truncated version of the linear congruential generator can indeed not be cryptanalyzed then it belongs to the class DRG.3.

\subsection{The Blum-Blum-Shub Generator}\label{gen:bbs}

The Blum-Blum-Shub generator was introduced in 1982 to the cryptographic community and later published in \cite{blublu86}. The generator produces a pseudorandom bit sequence from a random seed by repeatedly squaring modulo a so called \textit{Blum integer} $N = p \cdot q$, where $p$ and $q$ are distinct large random primes congruent to $3$ mod $4$. In its basic variant, in each round the least significant bit of the intermediate result is returned. \cite{vazvaz84} proved that the Blum-Blum-Shub generator is secure if $k = ~log_2 ~log_2 N$ least significant bits are output per round.

\cite{alecho88b} proved that factoring the Blum integer $N$ can be reduced to being able to guess the least significant bit of any intermediate square with non-negligible advantage. The output of this generator is thus cryptographically secure under the assumption that factoring Blum integers is a hard problem.

In our evaluation $p$ and $q$ are randomly selected $1024$ bit primes with $p = q = 3$ mod $4$, which corresponds --- as above --- to the security level of $128$ bits following again the \cite{bsi17-tr02102} guideline TR-02102-1.

If factoring Blum integers is hard then the Blum-Blum-Shub generator---properly seeded---is a DRG.3 generator in the terminology of \cite{bsi11}. 

\subsection{The RSA generator}
The RSA generator was first presented by \cite{sha83b} and is one of the
pseudorandom generators that are proven to be cryptographically secure under certain number-theoretical assumptions.
Analogously to the RSA cryptosystem, the generator is initialized by
choosing a modulus $N$ as the product of two large random primes, and an
exponent $e$ with $1 < e < \varphi(N)-1$ and $\gcd(e, \varphi(N)) = 1$. Here,
$\varphi$ denotes Euler's totient function.
Starting from a seed $x_0$ provided by an external source, the generator
iteratively computes
\begin{align*}
    x_{i+1} = x_i ^e \mod N,
\end{align*}
extracts the least significant $k$ bits of each intermediate result $x_i$ and concatenates them as output.

Our implementation uses a random 2048-bit Blum integer (see Section \ref{gen:bbs}) as modulus $N$ and various choices for the parameters $e$ and $k$.

In \cite{alecho88b} it is shown that the RSA generator is pseudorandom for $k = ~log_2 ~log_2 N = 11$, under the assumption that the RSA inversion problem is hard. For our tests, we choose $e = 3$, as for small exponents the generator is expected to work fast and because it allows us to compare the results to the runtime of the Blum-Blum-Shub generator.
 % (cf. Section \ref{gen:bbs}). 

Under a stronger assumption called the SSRSA assumption,  \cite{stepie06} prove the security of the generator for $k \leq n \cdot (\frac{1}{2} - \frac{1}{e} - \varepsilon - o(1))$ for any $\varepsilon > 0$, giving for $e = 3$ the parameter value $k = 238$. 
Additionally, we test the larger exponent $e = 2^{16} + 1$, which is widely used in practice,
 for it is a prime and its structure allows efficient exponentiation, with $k = 921$.

If the RSA inversion problem is hard then the RSA generator---properly seeded---is a DRG.3 generator in the terminology of \cite{bsi11}. 

\section{Evaluation}\label{s:eval}

To evaluate the efficiency of the generators considered, we developed a framework that  runs the software generators based on seed data from the PRG310-4.
To this end, we implemented the generators in C, using the  GMP library, see \cite{gmp14}, to accomplish large integer arithmetic.
The evaluation framework sequentially runs all generators,  reading from one true random source file of 512kB and producing 512kB each, while measuring the runtime of each generator and the byte entropy of each output.

All algorithms were run on an Acer V Nitro notebook with a Intel Core i5-4210U CPU at 1.70GHz with 8 GB RAM.
We used Ubuntu 16.04 64-bit with kernel version 4.10.0-32, as well version 4.4.0-92 as reference for the kernel random devices.

This process was repeated 750 times specifically, so that the average runtime of the generators should not deviate considerably from its expectation. 

To see this, let $A$ be a randomized algorithm. Then the runtime $t(A)$ is a
random variable. Without loss of generality let the runtime be bounded in
the interval $I = [0..1]$. We write $t = ~{\mathcal E} t(A)$ for the
expected runtime of $A$.  Consider running the randomized algorithm $k$
times. Then the average runtime of this experiment is 
$$X_k = \frac{1}{k} \cdot (t(A)_1 + ... + t(A)_k).$$
For its expectation we have
$$~{\mathcal E} X_k = ~{\mathcal E} t(A) = t \in I.$$
Thus, the expectation of the average runtime of $k$ runs is equal to
the expectation of a single run. 
If we observe after $k$ runs an average runtime of $X_k$, then we can ask: 
\begin{center}
How large should $k$ be so that the probability that the observed 
value $X_k$ significantly differs from its expectation $~{\mathcal E} X_k$
is very small?
\end{center} 
By Hoeffding's inequality we have
$$~prob(|X_k - ~{\mathcal E} X_k| \geq \delta) \leq \epsilon$$
for a real number $\delta \in \mathbb{R}_{> 0}$ and $\epsilon = 2 ~exp(-2k \delta^2)$.
To be statistically significant, we set $\epsilon = \delta = 0.05$, as typically done in statistics.
Then we require that $~prob(|X_k - t| \geq 0.05) \leq 0.05 = 2 ~exp(-2k \cdot 0.05^2)$, i.e., $k > 737$.

Thus we need at least 737 runs of the algorithm so that the
probability that the observed result deviates statistically significant
from the actual expected runtime is smaller than 1/20. Thus, 750 runs will
do the job.

In order to reduce the impact of other operating system components during our benchmarking, we decided to split up the initialization and generation processes and measure the time for the generation only after a certain amount of data was generated.
This way, the throughput of the generators had time to stabilize and we thus omit possible noise that is produced when the generator is started up.
To determine the appropriate amount of data to be generated before the measurement, we measured throughput for increasing amounts of data so that we could see at which point the throughput stabilizes.

\begin{figure}[htpb]
\centering
\includegraphics[height=150px]{final_software.eps}
\caption{Generator throughput after initialization for different output lengths.}
\label{fig:software}
\end{figure}

Figure \ref{fig:software} shows the pseudorandom software generators along with the two versions of \texttt{/dev/urandom} as reference points.
In the logarithmic scale on the throughput axis, an AES implementation on AES-friendly hardware 
has a throughput of about 2.5GB/s of pseudorandom data, while the RSA generator with 921 bit truncation and 65537 as public exponent, i.e., the fastest number-theoretic generator assumed to be secure, provides about 2.7MB/s of pseudorandom data. 
This makes the latter about 1000 times slower than the AES generator. The linear congruential generator can compete with the fast AES implementation, when not truncating the output, generating about 922MB/s, but as explained in Section \ref{sec:lcg} without truncation the generator is not cryptographically secure. 
As a fairer comparison to the textbook implementations of the number theoretic generators, the textbook version of AES still generates 32.7MB/s, beating the RSA implementation by a factor of ten.

\begin{figure}[htpb]
\centering
\includegraphics[height=150px]{final_hardware.eps}
\caption{Physical generator throughput after initialization.}
\label{fig:hardware}
\end{figure}

A second benchmark was performed for the different physical generators considered, depicted in Figure \ref{fig:hardware}.
Again a logarithmic scale is employed to allow having the \texttt{/dev/urandom} devices with up to 166.8MB/s of output and the \texttt{/dev/random} device with 2.2B/s of output in the same picture.
The most surprising observation is the jump in performance regarding the \texttt{/dev/urandom} device introduced by the re-implementation described in section \ref{gen:dev}.
When only considering blocking physical devices, i.e., taking out \texttt{/dev/urandom} completely, the Bergmann generator outperforms the \texttt{/dev/random} device easily both with (13kB/s) and without post-processing (29kB/s).

The amount of randomness needed for seeding the software generators differs considerably.
The least amount is needed by the AES based generators, which need 128 bits for the textbook and 256 bits for the OpenSSL implementation. 
The latter randomizes initial counter and key, whereas the former only randomizes the key.
Both the RSA and the Blum-Blum-Shub generator need to generate two 1024
bit primes.
The textbook method chooses uniformly random integers of the
appropriate size and tests them successively for compositeness.
This requires tests on expectedly $2 \cdot 1024 \cdot ~ln 1024 \approx 14200$ different integers by the prime number theorem, thus consuming approximately 1.8MB of seed randomness. The primality
tests themselves might consume additional randomness if a probabilistic variant is employed.
There are cheaper methods, though, reducing the necessary amount of
randomness to 2048 bits only.
\ifanonymous\else %
For details on this matter see \cite{loenus13a}.
\fi

The linear congruential generator additionally randomizes the initial state and thus consumes further 2048 bits for seeding.

Taking the throughput of the physical generators into account, the amount of time needed between possible reseeding ranges from $ \frac{128}{8}\cdot\frac{1}{29000} = 0.00055~s$ for the textbook AES generator seeded by the Bergmann generator to $ \frac{6144}{8}\cdot\frac{1}{2.2} = 349~s$ for the linear congruential generator seeded by \texttt{/dev/random}.

While the statistical quality of each generators output is not dependent on the reseeding, the amount of total entropy is not raised by any internal calculation, making regular reseeding sensible.
Using the Bergmann generator for seeding, even the linear congruential generator can be reseeded every $0.026~s$, which seems a reasonable time span especially in networking contexts.

\section{Conclusion and Future Work}\label{s:conc}\label{s:fw}
We implemented a number of software random generators and compared their performance to physical generators. 
A blocking \texttt{/dev/random} is way too slow to be of practical use as the only source of (pseudo-)randomness, except for seeding software generators.
The generator PRG310-4 is roughly as good as our Blum-Blum-Shub implementation. 
However, both are surpassed by the RSA generator when run with a fast parameter set, which offers the same level of  security. 

The most interesting result is the vast deviation between blocking and non-blocking random devices. This illustrates in a nice way the still open question whether lots of potentially bad randomness surpass the parsimonious use of guaranteed high-quality randomness.
 
The results also suggest a profitable symbiosis of hardware-generated seeds and number-theoretic high throughput---rather the reverse of the situation in other cryptographic contexts, say, the Diffie-Hellman exchange of keys for fast AES encryption.

The speedup introduced by the AES-NI instruction-set allows to generate 151 MB/s on a laptop computer, surpassing the requirements of the NGINX cluster (1.3 MB/s) by far, implying a negligible CPU-load.

Practical use of our findings has not taken place yet. Depletion of \texttt{/dev/random} is a realistic issue---workarounds for implied problems even suggest using the non-blocking \texttt{/dev/urandom} as a physical generator, see \cite{sea08}. 
However, prohibiting the use of \texttt{/dev/urandom} for key generation is also under debate, see \cite{bern14}, and there seems to be no consensus in the near future.

As a next step, implementing and testing on kernel level using optimized implementations is recommended.

Implementing an AES based random generator in the Linux kernel appears to be reasonable, but other platforms (\ie ARM) may favor other hardware-accelerated ciphers for better performance and less CPU load. Thus the cipher must be made configurable.

\section*{References}
%\vspace{-.3cm}
\bibliography{journals,refs,lncs,addition}

\end{document}

