%\errorcontextlines999
%\documentclass[noccpublish,balancedmargins,
%11pt,textarea=cc1,longnamesfirst,
%10pt,textarea=full,%twocolumn,onecolumnendtitle,
%multilingual,german,
%polutonikogreek,
%australian,english]{cc}
\documentclass[preprint,10pt,authoryear,multilingual,german]{elsarticle}
\let\dateenglish\dateaustralian

\usepackage{etex}% make sure that there are enough counters, ...
\usepackage{xcolor, soul}
%\usepackage[draft,dontshowlabels]{develop}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template Kryptotag, Version 2006-03-07
% Evtl. Kommentare, Aenderungen, ...
% bitte an Christopher Wolf
% krypto.AT.Christopher.HYPHEN.Wolf.DOT.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2006-03-07: full-page hinzugefügt
% 2005-02-06: Boolean für Bibliographie 
%             eingeführt
% 2004-07-21: erste Version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphics}
\usepackage{amsmath,amsfonts,amssymb,epsfig}
\usepackage{url}
\usepackage{german}
\usepackage{ifthen}
\usepackage{fullpage}
\usepackage{lscape}

% Nur fuer Randbenmerkungen
\usepackage{marginnote}
\usepackage[outer=6cm,marginparwidth=5.5cm]{geometry}

%\usepackage{tilde}

% If the trigger refsbib is true then the bibliography is generated 
% from refs.bib. Otherwise, the *previously generated* .bbl is simply
% read and put into the article
\newif\ifrefsbib
\refsbibfalse
% To toggle, uncomment the following line
\refsbibtrue

\usepackage{natbib}
\ifrefsbib
\bibliographystyle{cc2}
\fi

%\bibpunct{(}{)}{;}{author-year}{}{,}

\usepackage{booktabs}
\usepackage{caption}
\usepackage{threeparttable}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bitte keine neuen Pakete selbst
% einbinden sondern ggf. Ruecksprache
% mit dem Autor nehmen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\usepackage[utf8]{inputenc}
%\usepackage[T2A,T1]{fontenc}
%\usepackage[german,russian,english]{babel}

%\addto\captionsenglish{\renewcommand{\refname}{}}

\newcommand{\Ff}{\mathbb{F}}
\newcommand{\Ee}{\mathbb{E}}
\newcommand{\Nn}{\mathbb{N}}
\newcommand{\Zz}{\mathbb{Z}}
\newcommand{\eop}{\hspace*{\fill}$\Box$}
\newcommand{\eqdef}{\overset{\mathrm{def}}{=\joinrel=}}

\newcommand{\ie}{\textit{i.e.}}
\newcommand{\eg}{\textit{e.g.}}
\newcommand{\etAl}{\textit{et al.}}

 
\DeclareMathOperator{\parity}{parity} 

% versions of libraries used
%\input{versions}
% see documentation/plots/table_macros.py
\input{main_results}
\begin{document}

\begin{frontmatter}
\title{Comparative analysis of pseudorandom generators}





\begin{abstract}
    We run several selected software and hardware generators under controlled conditions and compare their throughput and consumed entropy. 
    In software, we distinguish between number theoretic generators (which come with corresponding security proofs) and generators based on AES. 
    Physical random generators covered by our study are the hardware generator PRG310-4, \texttt{/dev/random}, and \hl{\texttt{/dev/urandom}}
    \marginnote{\begin{scriptsize}Ich wuerde urandom nicht als hardware-genrator verstehen ... das ist zumindest gemischt.                                                                                                        \end{scriptsize}}  as implemented in the Linux kernel.
   
% Physical random generators covered by our study are the hardware generator PRG310-4 and \texttt{/dev/random} as implemented in the Linux kernel.
%Since \texttt{/dev/random} is fed by system events, we analyze both an idle lab environment and a server hosting several virtual machines.
%As examples for cryptographically secure RGs our analysis compares the RSA generator and the Blum-Blum-Shub generator, both for $3000$-bit moduli and suitable truncation of the outputs. 
%We include one RGs, namely a truncated linear congruential generator (LCG) whose security is questionable and for reference the untruncated linear congruential generator. To complete the picture of software RGs, we analyze how AES in counter mode performs. In order to obtain repeatable and comparable results, our implementations of the software RGs were all run on the same machine and produced 512~kB of output each, using AES post-processed output of the generator PRG310-4 as source for random seed bits. 

\hl{Our results indicate that software genators, including number-theoretic generators compete very well against physical ones,} 
especially when there are dedicated processor instructions for the generator available. The AES based generator employing the instruction set extension AES-NI outperforms all considered generators by far. 
Still, also the pure software-based AES-based generator has quite a good performance, 
\marginnote{\begin{scriptsize}Stil: Oben sprachen wir davon, dass wir Software und HW-Generatoren haben, nun sprechen wir von number-theoretic ones --- Die conclusio mag noch nicht so recht                                                                                                                                                                           \end{scriptsize}}
\hl{leading to the conclusion that choosing a cipher according to the platform instead of \texttt{/dev/random} and \texttt{/dev/urandom}, respectively, 
and seeding with a physical genrator respectively, gives a substantial speed-up in practice.}

 
%    \enlargethispage{.5cm}
%    \begin{table}[h]
%    \small
%      \centering
%        \renewcommand{\arraystretch}{1.2}
%        \begin{tabular}{l|cccc}
%                                              & byte entropy    & runtime           & throughput              & throughput        \\
%%                                              &                 & w/o init           & w/o init                 & w/o init           \\
%                                              & $[\text{bit}]$  & $[\mu \text{s}]$  & $[\text{kB}/\text{s}]$  & normalized        \\
%        \hline          
%        \hline
%          PRG310-4, no post-processing        & \bmPlainByteEnt & \bmPlainRuntimeW   & \bmPlainThruAbsW         & \bmPlainThruNormW  \\
%          \quad AES post-processing           & \bmAESByteEnt   & \bmAESRuntimeW     & \bmAESThruAbsW           & \bmAESThruNormW    \\
%          \texttt{/dev/random}, in the field  & \drFieldByteEnt & \drFieldRuntimeW   & \drFieldThruAbsW         & \drFieldThruNormW  \\
%          \quad in the lab                    & \drLabByteEnt   & \drLabRuntimeW     & \drLabThruAbsW           & \drLabThruNormW    \\
%        \hline
%%          Littlewood                          & \lwByteEnt      & \lwRuntime        & \lwThruAbs              & \lwThruNorm       \\
%          Linear congruential generator       & \lcgByteEnt     & \lcgRuntime       & \lcgThruAbs             & \lcgThruNorm      \\
%        \hline
%          Blum-Blum-Shub                      & \bbsByteEnt     & \bbsRuntime       & \bbsThruAbs             & \bbsThruNorm      \\
%          RSA, $e=2^{16}+1$, $1400$ bit/round & \rsafByteEnt    & \rsafRuntime      & \rsafThruAbs            & \rsafThruNorm     \\
%          \quad $e=3$, $1$ bit/round          & \rsaByteEnt     & \rsaRuntime       & \rsaThruAbs             & \rsaThruNorm      \\
%%          Nisan-Wigderson                     & \nwByteEnt      & \nwRuntime        & \nwThruAbs              & \nwThruNorm       \\
%        \end{tabular}
%        \caption{\small Overview of the results for generating 512 kB of output.} \label{tab:results}
%    \end{table}
\end{abstract}

\author{Johannes vom Dorp} 
\ead{dorp@cs.uni-bonn.de}

\author{Joachim von zur Gathen} 
\ead{gathen@bit.uni-bonn.de}
\author{Daniel Loebenberger} 
\ead{daniel@bit.uni-bonn.de@genua.de}
\author{Jan L\"{u}hr} 
\ead{luehr@cs.uni-bonn.de}
\author{Simon Schneider} 
\ead{schneid@cs.uni-bonn.de}

\end{frontmatter}


%\end{frontmatter}

%\maketitle

\selectlanguage{english}

\section{Introduction}\label{s:int}
A crucial ingredient of almost every commonly used cryptographic scheme is the internal choice of random bits, e.g. for key generation. This includes the indistinguishability of the generated bits from truly random  bits
and hence the absence of statistical abnormalities within the random sequence of output. Doing so is not trivial.

One approach is to use system events (keyboard, mouse, etc.). On Linux systems this is realized via the devices \texttt{/dev/random} and \texttt{/dev/urandom}. While \texttt{/dev/random} blocks if not enough randomness is available on the system, \texttt{/dev/urandom} uses a pseudorandom  generator to generate output, see \cite{gutpin06} and \cite{lac12}.

A common measure for the amount of available randomness is the \emph{entropy} of a random source. 
Having enough entropy is vital for any kind of cryptographic use of random bits. 
For instance, not having enough entropy caused severe weakening of Debian's OpenSSL implementation \cite{sch08g}. 


\marginnote{\begin{scriptsize}Ich find den Verweis auf das Debian OpenSSL fiasko etwas irrefuehrend. Wir argumentieren ja auch primaer pro Performance und nicht pro Entropy.
Wir koennen den Verweis drin lassen ... weil, wir da Begruendung k�nnen, welche Auswirkungen unsicherer Zufall hat, aber ich wuerde noch ein Hinweis zum Sizing mit aufnehmen...
Bitte checkt nochmal die Zahlen            \end{scriptsize}}

\hl{When handling up to 43,000 new TLS transactions per second (ECC 256 bit, ECDHE-ECDSA-AES256-GCM-SHA384) on a single, Intel Xeon based system} (cf. \cite{nginx} product information)
\hl{1376 KB/s of entropy are consumed to generate a pre-master secret fo 256 bit each ---ideally, using only negligible CPU resources.}

Thus, modern computer systems  desperately need both, cryptographically secure pseudorandom generators with high performance and a physical entropy source, which delivers seed entropy at acceptable speed. The view that pseudorandom generators are to be considered as separate cryptographic primitives is by far not new, see \cite{kelsch98}.

This article provides a comparative analysis of some popular pseudorandom generators and two physical sources of randomness. Of course, the choice of possible generators is vast. We thus tried to carefully select representatives for the respective classes to get a representative picture of the whole situation. As far as we know, such an analysis was never done before. A preliminary version of this work can be found in \cite{burdor15}.

In our setup, we use as a source of random seeds one particular output of the hardware generator PRG310-4, which was analyzed in \cite{schkil03}. On the software side we discuss several number-theoretic generators, namely the (in general non-cryptographic) linear congruential generator, the RSA generator, and the Blum-Blum-Shub generator, all at carefully selected truncation rates of the output. The generators come with certain formal reductionist arguments. For comparison we add to our analysis pseudorandom generators based on a well-studied block cipher, in our case AES in counter mode.





The article is structured as follows: We first present previous work in the field of generator analysis in \ref{s:rw} before giving a detailed overview of the generators in \ref{s:gen}. The main contribution will be the evaluation regarding throughput and entropy usage in \ref{s:eval}. We conclude in \ref{s:conc}.

All algorithms were implemented in a textbook manner using \hl{non-optimized C-code} \marginnote{\begin{scriptsize}Gilt nicht fuer AES-NI                                                                                                                       \end{scriptsize}}, thus providing a fair comparison. The source code of the algorithms is available at \cite{burdor15a}.
% JvzG: n�chster Satz ok?
%This paper is the result of a class project in a course taught by Joachim von zur Gathen and Daniel Loebenberger.

\section{Related work}\label{s:rw}

Concerning the physical generators, \cite{schkil03} analyzed noisy diodes as a random source, proving a model for its entropy. One example of a noisy diodes based generator is the generator PRG310-4, which is distributed by \cite{ber14}. Linux' VirtIO generator as used in  \texttt{/dev/random} is illustrated by \cite{gutpin06} and explained by  \cite{lac12}. Both provide a clear picture of its inner workings. 

Referring to pseudorandom generators, the RSA based generator is explained in \cite{sha83b}, \cite{fissch00}, and \cite{stepie06}. Its cryptographic security is shown in \cite{alecho88} and extended in \cite{stepie06}. 
Linear congruential generators were first proposed by \cite{leh51}. Attacks were discussed in \cite{boy89}, \cite{plu82b}, and \cite{has85}. They all exploited its simple linear structure and come with a specific parameterization. Not all parameterizations --- such as truncating its output to a single bit --- have been attacked successfully as of today. \cite{conshp05} analyze this in depth concluding that (for some cases)
\begin{quotation}
we do not know if the truncated linear congruential generator can still be cryptanalyzed.
\end{quotation}
\cite{blublu86} introduced the Blum-Blum-Shub generator. \cite{alecho88} and \cite{fissch00} showed that the integer modulus can be factored, given a  break of the generator. 

A totally different approach for the construction of pseudorandom generators are the ones based on established cryptographic primitives. \cite{nist12-800-90A} specifies several standards for producing cryptographically secure random numbers. Besides hash-based techniques, there is also a standard employing a block cipher in counter mode, see also \cite{nist01-800-38A} for this purpose. 

\marginnote{\begin{scriptsize}Hier nochmal Anwendungsbezug - ich find das RFC wichtig, weil es viel zitiert und eben auch ein Standard ist)                                                                                                                         \end{scriptsize}}
\hl{In RFC 4068} \cite{RFC4086} \hl{compare different techniques and provide a de-facto standard focussing internet engineering. 
For PRNGs cryptographically strong sequences and entropy pool techniques are proposed. RFC 4068 lacks recommendations
for the ciphers to be used in OFB and CTR generation. }

We are not aware of any comprehensive benchmarking survey for these generators that integrates them into the Linux operating system.

\section{The generators}\label{s:gen}
In the following, each generator which was implemented or applied for the comparative analysis is briefly presented. The output of a pseudorandom generator is, by definition, not efficiently distinguishable from uniform randomness. Under suitable---yet reasonable---assumptions, the Blum-Blum-Shub, and RSA generators with suitable truncation have this property under certain assumptions, but the linear congruential generator and the \hl{AES based generator do not}.
\marginnote{\begin{scriptsize}Huh? Unter reasonable Annahmen, kannst Du nicht davon ausgehen, ist unterscheidbaar von Randomness?? Dann waehre AES im CTR mode ja kaputt, weil man einen predictor bauen koennte.
Ich denke, hier sollten wir schon sagen, welche Annahmen das sein sollen.                                                                         \end{scriptsize}}

% As the two generators in question have been exposed to be predictable, this definition does not apply to them.

\subsection{Linux \texttt{/dev/random}}
%VirtIO}
The \emph{Bundesamt f\"ur Sicherheit in der Informationstechnik} (BSI) sets cryptographic standards in Germany and judges both \texttt{/dev/random} and \texttt{/dev/urandom} to fulfill the requirements for their class NTG.1,  except for NTG.1.1 which is not satisfied by \texttt{/dev/urandom}, see \cite{bsi-linux-rng}. The definitions of the classes for random number generation can be found in  \cite{bsi11}.
As already mentioned system events are used to gather entropy on Linux Systems. \cite{lac12} explains: 
\marginnote{\begin{scriptsize}Was heisst das nun ganz konkret - was ist die Konsequenz? Das vers. API-Methoden in verschieden BISKlassen sind, sagt erstmal nur, dass sie verschieden sind.
Mir fehlt in dem Artikel auch ein wenig die Kritik an urandom. Afair ist der Code recht undurchschaubar und es ``damals'' durch verschiedene
US-Exportresktriktionen fuer starke Crypto etwas komisch gewachsen. Habt Ihr die Kritik mit Absicht heraus genommen? Die Kritik, dass trotz mangelenden Seeds Zufall generiert wird, ist fuer mich ein eher technisches Detail, -- die Frage w�re eher, ab wenn genug Entropie da 
da ist und wieviel benoetigt wird. I.a. kann man jeden PRNG so dumm implementieren, dass er trotz fehlender Seed-Entropie irgendwas generiert. Das ist jetzt kein Alleinstellungsmerkmal von urandom.                                                                                                                                                                  \end{scriptsize}}
\begin{quotation}
  ``[Linux] processes events from different entropy sources, namely user inputs (such
as keyboard and mouse movements), disk timings and interrupt timings.''
\end{quotation}
These events are post-processed and made available to \texttt{/dev/random} and \texttt{/dev/urandom}. This includes estimating the entropy of the event and mixing.


\subsection{PRG310-4}
The PRG310-4 gathers entropy from a system of two noisy diodes, see \cite{ber14}, and is connected
to a computer via USB. Similar variants exist for different interface types. According to  \cite{ber14}, its behavior follows the stochastic model in  \cite{schkil03}, who show that 
 \begin{quotation}
   ``[...] principally, this RG [design] could generate up to $700000$ random bits per second with an average entropy
$1-10^{-5}$.''
 \end{quotation}
\cite{ber14} mentions that this device satisfies all requirements for class PTG.3 for random generation.

\subsection{AES in counter mode}
Due to the fact that since 2008 there is AES-NI, for a white paper see \cite{gue10}, realizing dedicated processor instructions on Intel and AMD processors for the block cipher AES as standardized by \cite{fips197}, we add to our comparison the AES counter mode generator. This generator is standardized by \cite{nist12-800-90A} and produces a sequence of $128$ bit blocks. We aim at security level of $128$ bits thus employing AES-128 as the underlying block cipher.

The security of the AES generator directly reduces to the security of AES. Indeed, any distinguisher for the pseudorandom generator gives an equally good distinguisher for AES in counter mode. Assuming the latter to be secure, one concludes that also the pseudorandom generator is also secure. 

However, in contrast to the number-theoretic generator described below, we do not have any reductionist argument in our hands to actually prove that the generator is secure if some presumably hard mathematical problem is intractable. We need to trust that the cipher AES is secure---and the dedicated processor instructions on our CPU.

\subsection{Linear Congruential generators}
The linear congruential generator as presented in \cite{leh51} produce for $i \geq 0$ a sequence of values in $x_i \in \mathbb{Z}_M$, generated by applying for $a,b \in \mathbb{Z}_M$ iteratively
\begin{equation*}
  x_i = a \cdot x_{i-1} + b  \text{ in $\mathbb{Z}_M$}
\end{equation*}
to a secret random seed $x_0 \in \mathbb{Z}_M$ provided by an external source. The parameters $a$ and $b$ and $M$ are also kept secret and chosen from the external source.

While the byte distribution of linear congruential generator outputs is generally well-distributed, with byte entropy close to maximal, the generated sequences are predictable and therefore cryptographically insecure. 
%A paper on predicting pseudorandom sequences in general 

The authors of the two papers \cite{plu82b} and \cite{boy89} describe how to recover the secrets $a$, $b$ and $M$ from the sequence of $(x_i)_{i \geq 0}$ alone.
A possible mitigation against this attack is to output only some \emph{least significant bits} of the $x_i$.
% \citet{has85} describes a lattice based attack on truncated LCGs where $(s,t,M)$ are public.
\cite{has85} describe a lattice based attack on truncated linear congruential generator where all parameters are public. \cite{ste87} showed that also in the case when the parameters are kept secret.
This attack can be used to predict linear congruential generators that output at least $\frac{1}{3}$ of the bits of the $x_i$. \cite{conshp05} write that there is no cryptanalytic attack known when only approximately $k = ~loglog_2 M$ bits are output per round.

We are neither aware of a more powerful attack on the linear congruential generator nor of a more up-to-date security argument for truncated linear congruential generators.

In our evaluation we used a prime modulus $M$ with 2048 bit, which corresponds to a security level of $128$ bits according to the \cite{bsi-tr-02102-1-2015} guideline TR-02102-1. Per round we output $k = 11$ bits, which coincides with the value from \cite{conshp05} mentioned above. For comparison, we also run the generator with modulus $M = 2^{2048}$ and full output, that is, no truncation, which is basically the fastest number-theoretic generator we can hope for.

\subsection{The Blum-Blum-Shub Generator}\label{gen:bbs}

The Blum-Blum-Shub generator was introduced in 1982 to the cryptographic community and later published in \cite{blublu86}. The generator produces a pseudorandom bit sequence from a random seed by repeatedly squaring modulo a so called \textit{Blum integer} $N = p \cdot q$, where $p$ and $q$ are large random primes congruent to $3$ mod $4$. In its basic variant, in each round the least significant bit of the intermediate result is returned. \cite{vazvaz84} proved that the Blum-Blum-Shub generator is secure if $k = ~loglog_2 N$ least significant bits are output per round.

\cite{alecho88} proved that factoring the Blum integer $N$ can be reduced to being able to guess the least significant bit of any intermediate square with non-negligible advantage. The output of this generator is thus cryptographically secure under the assumption that factoring Blum integers is a hard problem.

In our evaluation $p$ and $q$ are randomly selected $1024$ bit primes with $p = q = 3$ mod $4$, which corresponds --- as above --- to the security level of $128$ bits following the \cite{bsi-tr-02102-1-2015} guideline TR-02102-1.

\subsection{The RSA generator}
The RSA generator was first presented by \cite{sha83b} and is one of the
PRGs that are proven to be cryptographically secure under certain number-theoretical assumptions.
Analogously to the RSA crypto scheme, the generator is initialized by
choosing a modulus $N$ as the product of two large random primes, and an
exponent $e$ with $1 < e < \varphi(N)-1$ and $\gcd(e, \varphi(N))$. Here,
$\varphi$ denotes Euler's totient function.
Starting from a seed $x_0$ provided by an external source the generator
iteratively computes
\begin{align*}
    x_{i+1} = x_i ^e \mod N,
\end{align*}
extracts the least significant $k$ bits of each intermediate result and
concatenates them as output.

Our implementation uses a random 2048-bit Blum integer (see \ref{gen:bbs}) as modulus $N$ and various choices for the parameters $e$ and $k$.

In \cite{alecho88} it is shown that the RSA generator is pseudorandom for $k = ~loglog_2 N = 11$, under the assumption that the RSA inversion problem is hard. For our tests, we choose $e = 3$, as for small exponents the generator is expected to work fast and because it allows us to compare the results to the runtime of the Blum-Blum-Shub generator.
 % (cf. section \ref{gen:bbs}). 

Under a stronger assumption called the SSRSA assumption,  \cite{stepie06} prove the security of the generator for $k \leq n \cdot (\frac{1}{2} - \frac{1}{e} - \varepsilon - o(1))$ for any $\varepsilon > 0$, giving for $e = 3$ parameter $k = 238$. Additionally, we test the larger exponent $e = 2^{16} + 1$, which is widely used in practice for it is a prime and its structure allows efficient exponentiation, with $k = 921$.

\section{Evaluation}\label{s:eval}
To evaluate efficiency as well as statistical properties for the presented generators,
we developed a framework that  runs the software generators based on seed data from the PRG310-4.
To this end, we implemented the generators in C, using the  GMP library, see \cite{gmp14}, to accomplish large integer arithmetic.
The evaluation framework sequentially runs all generators,  reading from one true random source file of 512kB and producing 512kB each, while measuring the runtime of each generator and the byte entropy of each output.

All algorithms were run on a Lenovo ThinkPad T530 with a Intel Core i7-3610QM @ 2.30GHz CPU with 12 GiB RAM. 
We used Linux Mint 17.1 Cinnamon 64-bit, Linux 3.13.0-55-generic, glibc 2.19, libgmp 5.1.3, libmpfr 3.1.2-p3 and gcc 4.8.4.
TODO{Update machine!}

\subsection{Generator throughput}
\begin{figure}[p]
\centering
\includegraphics[width=\textwidth]{software.eps}
\caption{Software generator throughput after initialization for different output lengths.}
\label{fig:software}
\end{figure}

In an attempt to increase comparability, we decided to split up the initialization and generation processes and measure the time for the generation only after a certain amount of data was generated.
This way, the throughput of the generators had time to stabilize and we thus omit possible noise that is produced when the generator is first started, see \ref{fig:software}.

To determine the appropriate amount of data to be generated before the measurement, we measured throughputs for increasing amounts of data so that we could see at which point the throughputs stabilize.

A second benchmark was performed for the different hardware generators considered. The result is depicted in \ref{fig:hardware}.

\begin{figure}[p]
\centering
\includegraphics[width=.8\textwidth]{hardware.eps}
\caption{Hardware generator throughput after initialization for different output lengths.}
\marginnote{TODO Groessenverhaeltnis Schrift / Balken und ggf. andere Beschriftung auf der y-Achse.}
\label{fig:hardware}
\end{figure}

%As the measurements in table \ref{fig:software} show, all generators stabilize after the generation of at least $1$ MB, such that we used this value as offset from which on to measure the generation time.
%The warm start measurements more closely related to actual use cases, since PRGs output streams of data. %Herbei argumentiert

%To compare the two generation variants we have listed throughputs for both, along with byte entropy and normalized throughput of the additional measurements in Table \ref{tab:warm}. For parameter generation, only the fast variant of RSA actually generated a Blum integer. The variants RSA slow, as well as the Blum-Blum-Shub generator then used the computed value, explaining the considerable change for the timings of the slow variant of the RSA generator. Also for the linear congruential generator a prime modulus was generated. Note that Table \ref{tab:results} given in the abstract only lists the results for the generators \emph{with} initialization.
%
%%%%%%%% Warm start measurements %%%%%%%
%   \begin{table}[tb]
%    \small
%      \centering
%        \renewcommand{\arraystretch}{1.2}
%        \begin{tabular}{l|cccc}
%                                           & byte entropy   & throughput             & throughput             & throughput      \\
%                                           &                & w/o init               & w/ init                & w/o init        \\
%                                           & $[\text{bit}]$ & $[\text{kB}/\text{s}]$ & $[\text{kB}/\text{s}]$ & normalized      \\
%        \hline          
%        \hline
%          PRG310-4, AES post-processing    & \bmAESByteEnt  & ---                    & \bmAESThruAbsW         & \bmAESThruNormW \\
%          \texttt{/dev/random}, in the lab & \drLabByteEnt  & ---                    & \drLabThruAbsW         & \drLabThruNormW \\
%        \hline
%%          Littlewood                       & \lwByteEnt     & \lwThruAbs             & \lwThruAbsW            & \lwThruNorm     \\
%          Linear congruential generator    & \lcgByteEnt    & \lcgThruAbs            & \lcgThruAbsW           & \lcgThruNorm    \\
%        \hline
%          Blum-Blum-Shub                   & \bbsByteEnt    & \bbsThruAbs            & \bbsThruAbsW           & \bbsThruNorm    \\
%          RSA, fast                        & \rsafByteEnt   & \rsafThruAbs           & \rsafThruAbsW          & \rsafThruNorm   \\
%          \quad slow                       & \rsaByteEnt    & \rsaThruAbs            & \rsaThruAbsW           & \rsaThruNorm    \\
%%          Nisan-Wigderson                  & \nwByteEnt     & \nwThruAbs             & \nwThruAbsW            & \nwThruNorm     \\
%        \end{tabular}
%        \caption{\small Overview of the results for warm-starting generators (w/o init). Cold-start (w/ init) throughput as comparison.} \label{tab:warm}
%    \end{table}

\subsection{Statistical properties of the generated data}
The NIST test suite, see \cite{sts10}, is a collection of statistical tests, aimed at checking blocks of pseudorandomly generated data for statistical irregularities. 
The user specifies the tests to be run on the data, as well as the parameters of the tests and the block size used for processing the data. 
The output is an enumeration of the applied tests, the number of blocks for which each 
test succeeded and a $p$-value, providing a confidence level for the correctness of the test results. 
For each test, the NIST specification gives a minimum $p$-value that indicates whether the input data should be regarded as random.

For data to be considered statistically random, a minimal number of blocks must pass various tests successfully. 
Each test has its own minimal block size. For instance, Maurer's universal test, see \cite{mau92}, requires blocks to be roughly 300kB at least.

With suitable parameters, all generators did pass all tests. This does not come as a surprise. The fact that the cryptographically insecure linear congruential generator passes these tests with flying colors substantiates the well-known opinion that such tests are of little cryptographic significance.

\section{Conclusion}\label{s:conc}
We implemented a number of software random generators and compared their performance to physical generators. \texttt{/dev/random} 
is way too slow to be of practical use\hl{, except for seeding PRNGs}. The generator PRG310-4 is roughly as good as our Blum-Blum-Shub implementation. However, both are surpassed by the 
RSA generator when run with a fast parameter set, which offers the same level of  security. 

The most interesting --- and surprising to us --- result is that number-theoretic methods outperform hardware-based approaches by far. 
Their additional advantage is security under standard complexity assumptions such as the hardness of factoring certain integers. However, they still need random seeds. 
This suggests a profitable symbiosis of hardware-generated seeds and number-theoretic high throughput --- 
rather the reverse of the situation in other cryptographic contexts, say, the Diffie-Hellman exchange of keys for fast AES encryption.

\hl{The speedup introduced by the AES-NI instruction-set allows to generate 151 MByte/s on an old laptop, surpassing the requirements of the NGINX cluster by far.}

\section{Future work}\label{s:fw}
We decided on parameter choices that look reasonable to us, but many alternatives are possible. Do other parameter settings lead to qualitatively different results?

Practical use of our findings has not taken place yet. Depletion of \texttt{/dev/random} is a realistic issue --- 
workarounds for implied problems even suggest using \texttt{/dev/urandom} as a physical generator, see \cite{sea08}. 
However, prohibiting the use of \texttt{/dev/urandom} for key generation is under debate, see \cite{bern14}. From the BSI's point of view, 
\texttt{/dev/urandom} fulfills all requirements for NTG.1 as defined in \cite{bsi11}, except for NTG.1.1, see \cite{bsi-linux-rng}.

Given benchmarking results and consumed entropy, using \texttt{/dev/random} for seed generation and then running a secure software PRG looks promising.
Kernel based implementations of the algorithms we investigated are not available as of today. Benchmarking and field testing are yet to be done.
As a next step, implementing and testing on kernel level using optimized implementations is recommended.
% Bibliography

Implementing an AES based PRNG in the Linux kernel (\ie \texttt{/dev/arandom}) appears to be reasonable, but other platforms (\ie ARM) may favor Salsa20 
for better performance and less CPU load on small IoT devices.


\section*{References}
\vspace{-.3cm}
\bibliography{journals,refs,lncs,addition}
\end{document}
