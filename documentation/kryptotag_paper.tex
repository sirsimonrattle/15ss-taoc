\documentclass[11pt]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template Kryptotag, Version 2006-03-07
% Evtl. Kommentare, Aenderungen, ...
% bitte an Christopher Wolf
% krypto.AT.Christopher.HYPHEN.Wolf.DOT.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2006-03-07: full-page hinzugefügt
% 2005-02-06: Boolean für Bibliographie 
%             eingeführt
% 2004-07-21: erste Version
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphics}
\usepackage{amsmath,amsfonts,amssymb,epsfig}
\usepackage{url}
\usepackage{german}
\usepackage{ifthen}
\usepackage{fullpage}
\usepackage{lscape}

% If the trigger refsbib is true then the bibliography is generated 
% from refs.bib. Otherwise, the *previously generated* .bbl is simply
% read and put into the article
\newif\ifrefsbib
\refsbibfalse
% To toggle, uncomment the following line
\refsbibtrue

\usepackage{natbib}
\ifrefsbib
\bibliographystyle{cc2}
\fi

%\bibpunct{(}{)}{;}{author-year}{}{,}

\usepackage{booktabs}
\usepackage{caption}
\usepackage{threeparttable}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bitte keine neuen Pakete selbst
% einbinden sondern ggf. Ruecksprache
% mit dem Autor nehmen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% nach Rücksprache eingebunden:
\usepackage[utf8]{inputenc}
\usepackage[T2A,T1]{fontenc}
\usepackage[german,russian,english]{babel}

\addto\captionsenglish{\renewcommand{\refname}{}}

\newcommand{\Ff}{\mathbb{F}}
\newcommand{\Ee}{\mathbb{E}}
\newcommand{\Nn}{\mathbb{N}}
\newcommand{\Zz}{\mathbb{Z}}
\newcommand{\eop}{\hspace*{\fill}$\Box$}
\newcommand{\eqdef}{\overset{\mathrm{def}}{=\joinrel=}}

\newcommand{\ie}{\textit{i.e.}}
\newcommand{\eg}{\textit{e.g.}}
\newcommand{\etAl}{\textit{et al.}}

\newcommand{\ktAffil}[1]{\ensuremath{#1}}
\newboolean{ktBibliography}
\newenvironment{ktArticle}[6]
  {\newpage
   \selectlanguage{#1}
   \begin{center}
     \Large #2
   \end{center}
   \begin{center}
     #3\\[0.5em]
     #4\\[0.5em]
   \end{center}
   #5
   \ifthenelse{\boolean{ktBibliography}}{\small \begin{thebibliography}{\hspace{1cm}} #6 \end{thebibliography}}{}
  }
  {}

% versions of libraries used
%\input{versions}
% see documentation/plots/table_macros.py
\input{main_results}


\DeclareMathOperator{\parity}{parity} 
\begin{document}
\newpage
% verwendete Sprache. Erlaubt: german oder english
\selectlanguage{english}

% Titel
\begin{center}
  \Large Comparative analysis of pseudorandom generators
\end{center}

% Name & Affiliation
\begin{center}
  Aleksei~Burlakov,
  Johannes~vom~Dorp,
  Joachim~von~zur~Gathen,
  Sarah~Hillmann,
  Michael~Link,
  Daniel~Loebenberger,
  Jan~Lühr,
  Simon~Schneider \&
  Sven~Zemanek
  \\[0.5em]
  \begin{tabular}{c}
    \url{{burlakov,dorp,luehr,schneid,zemanek}@cs.uni-bonn.de}\\    
    \url{{sarah.hillmann,michael.link}@uni-bonn.de}\\
    \url{{gathen,daniel}@bit.uni-bonn.de}\\
    Bonn-Aachen International Center for Information Technology \\
    Dahlmannstr. 2, Bonn \\
  \end{tabular}
  \\[0.5em]
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%
% Hauptteil
%%%%%%%%%%%%%%%%%%%%%%%%%

    We compare random  generators (RGs) under controlled conditions regarding their efficiency and statistical properties. 
    For this purpose, we distinguish between physical RGs and software RGs, which can be further subdivided into cryptographically secure and insecure RGs.
    
Physical RGs covered by our study are the hardware generator PRG310-4 and \texttt{/dev/random} as implemented in the Linux kernel.
Since \texttt{/dev/random} is fed by system events, we analyze both an idle lab environment and a server hosting several virtual machines.
As examples for cryptographically secure RGs our analysis compares the RSA generator and the Blum-Blum-Shub generator, both for $3000$-bit moduli. 
Additionally, we compare them to the Nisan-Wigderson construction with suitably selected parameters. 
We include two cryptographically insecure RGs, namely a linear congruential generator (LCG) and the Littlewood generator.

 In order to obtain repeatable and comparable results, our implementations of the software RGs were all run on the same machine and produced 
 512~kB of output each, using AES post-processed output of the generator PRG310-4 as source for random seed bits. 
 We compare the results in terms of byte entropy and throughput \emph{excluding initialization}. For further statistical analysis --- not shown in the table --- 
 we apply the NIST test suite on the outputs.
 
 The most important finding is that in our scenarios, number-theoretic generators compete very well against hardware-based ones.
    
    \enlargethispage{.5cm}
    \begin{table}[h]
    \small
      \centering
        \renewcommand{\arraystretch}{1.2}
        \begin{tabular}{l|cccc}
                                              & byte entropy    & runtime           & throughput              & throughput        \\
%                                              &                 & w/o init           & w/o init                 & w/o init           \\
                                              & $[\text{bit}]$  & $[\mu \text{s}]$  & $[\text{kB}/\text{s}]$  & normalized        \\
        \hline          
        \hline
          PRG310-4, no post-processing        & \bmPlainByteEnt & \bmPlainRuntimeW   & \bmPlainThruAbsW         & \bmPlainThruNormW  \\
          \quad AES post-processing           & \bmAESByteEnt   & \bmAESRuntimeW     & \bmAESThruAbsW           & \bmAESThruNormW    \\
          \texttt{/dev/random}, in the field  & \drFieldByteEnt & \drFieldRuntimeW   & \drFieldThruAbsW         & \drFieldThruNormW  \\
          \quad in the lab                    & \drLabByteEnt   & \drLabRuntimeW     & \drLabThruAbsW           & \drLabThruNormW    \\
        \hline
          Littlewood                          & \lwByteEnt      & \lwRuntime        & \lwThruAbs              & \lwThruNorm       \\
          Linear congruential generator       & \lcgByteEnt     & \lcgRuntime       & \lcgThruAbs             & \lcgThruNorm      \\
        \hline
          Blum-Blum-Shub                      & \bbsByteEnt     & \bbsRuntime       & \bbsThruAbs             & \bbsThruNorm      \\
          RSA, $e=2^{16}+1$, $1400$ bit/round & \rsafByteEnt    & \rsafRuntime      & \rsafThruAbs            & \rsafThruNorm     \\
          \quad $e=3$, $1$ bit/round          & \rsaByteEnt     & \rsaRuntime       & \rsaThruAbs             & \rsaThruNorm      \\
          Nisan-Wigderson                     & \nwByteEnt      & \nwRuntime        & \nwThruAbs              & \nwThruNorm       \\
        \end{tabular}
        \caption{\small Overview of the results for generating 512 kB of output.} \label{tab:results}
    \end{table}
    
    \newpage
    

\section{Introduction}\label{s:int}
A crucial ingredient of almost every commonly used cryptographic scheme is the internal choice of random bits for key generation. This includes the unpredictability 
and hence the absence of statistical regularities within the random sequence of output. Doing so is not trivial.

One approach is to use system events (keyboard, mouse, etc.) for gathering entropy. This is done on Linux systems and made available via the devices \texttt{/dev/random} and \texttt{/dev/urandom}. While \texttt{/dev/random} blocks if no entropy is available on the system, \texttt{/dev/urandom} uses a pseudorandom  generator (PRG) to generate output, see \citet*{gutpin06,lac12}.

Having enough entropy and randomness is \emph{vital} for any kind of key generation --- especially for systems relying on 
Diffie-Hellman key exchange protocols to achieve perfect
forwarding secrecy. For instance, not having enough randomness causes TLS based online banking to stop and emails to be held back. Thus, modern computer systems  desperately need  cryptographically secure PRGs 
% JvzG: was soll das bedeuten? --- as an alternative to existing ones --- 
and hardware entropy sources, delivering seed entropy to PRGs 
or key generators with very high speed.

This paper provides a comparative analysis of some popular pseudorandom generators and two physical sources of randomness.
As source of random seeds we use the PRG310-4
% hereinafter also referred to as Bergmann generator,
% and the \texttt{/dev/random} device.
Concerning PRGs, we discuss the RSA generator, the Blum-Blum-Shub generator and the Nisan-Wigderson construction, which come with formal security proofs, as well as the Littlewood generator and the linear congruential generator as examples of insecure generators in the context of cryptographic applications.

Therefore, we first present previous work in the field of generator analysis in section \ref{s:rw} before giving a detailed overview of the generators in section \ref{s:gen}.
The main contribution will be the evaluation regarding efficiency and statistical uniformity in section \ref{s:eval}, followed by a short conclusion in section \ref{s:conc}.
The last section \ref{s:fw} gives some hints on future work, building on the findings of this paper.

\textit{All} algorithms were implemented in a textbook manner using non-optimized C-code, thus providing a fair comparison. 
The source code of the algorithms is available at \citet*{burdor15a}.
% JvzG: n�chster Satz ok?
This paper is the result of a class project in a course taught by Joachim von zur Gathen and Daniel Loebenberger.

\section{Related work}\label{s:rw}

Concerning the physical generators, \citet{schkil03} analyzed noisy diodes as a random source, proving a model for its entropy. One example of a noisy diodes based generator is the generator PRG310-4, which is distributed by \citet{ber14}. Linux' VirtIO generator as used in  \texttt{/dev/random} is illustrated by \citet{gutpin06} 
and explained by  \citet{lac12}. Both provide a clear picture of its inner workings. 

Referring to PRGs, the RSA based generator is explained in \citet*{sha83b,fissch00,stepie06}. Its cryptographic security is shown in \citet*{alecho88} and extended in \citet{stepie06}. 

Linear congruential generators (LCGs) were first proposed by \citet{leh51}. Attacks were discussed in \citet{boy89, plu82b, has85}. They all exploited its simple linear structure and come with a specific 
parameterization. Not all parameterizations --- such as truncating its output to a single bit --- have been attacked successfully as of today.

\citet*{blublu86} introduced the Blum-Blum-Shub generator. \citet{alecho88} and \citet{fissch00} showed that the integer modulus can be factored, given a  break of the generator.

The generator by \citet{niswig94} is proven to be cryptographically secure against constant depth circuits. 

In the chapter  ``Mathematics with Minimum Raw Materials'' of his book ``A Mathematicians Miscellany'',  \citet{lit53} proposes what is today called a stream cipher. This construction can be interpreted as a random generator.

We are not aware of any comprehensive benchmarking survey for these generators that integrates them into the Linux operating system.


\section{The generators}\label{s:gen}
In the following, each generator which was implemented or applied for the comparative analysis is briefly presented.
The output of a pseudorandom generator is, by definition, not efficiently distinguishable from uniform randomness.
Under suitable yet reasonable assumptions, the Blum-Blum-Shub, RSA, and Nisan-Wigderson generators have this property,
but the LCG with full output and the Littlewood construction definitely do not.

% As the two generators in question have been exposed to be predictable, this definition does not apply to them.

\subsection{Linux \texttt{/dev/random}}
%VirtIO}
As already mentioned system events are used to gather entropy on Linux Systems. \citet{lac12} explains: 
\begin{quotation}
  ``[Linux] processes events from different entropy sources, namely user inputs (such
as keyboard and mouse movements), disk timings and interrupt timings.''
\end{quotation}
These events are post-processed and made available to \texttt{/dev/random} and \texttt{/dev/urandom}. This includes estimating the entropy of the event and mixing.

The \emph{Bundesamt f\"ur Sicherheit in der Informationstechnik} (BSI) sets cryptographic standards in Germany
and judges both \texttt{/dev/random} and \texttt{/dev/urandom} to fulfill the requirements for their class NTG.1,  except for NTG.1.1 which is not satisfied by \texttt{/dev/urandom}, see \citet*{bsi-linux-rng}. The definitions of the classes for random number generation can be found in  \citet{bsi11}.


\subsection{PRG310-4}
The PRG310-4 gathers entropy from a system of two noisy diodes, see \citet{ber14}, and is connected
to a computer via USB. Similar variants exist for different interface types.

According to  \citet{ber14}, its behavior follows the stochastical model in  \citet{schkil03}, who show that 
 \begin{quotation}
   ``[...] principally, this RG [design] could generate up to $700000$ random bits per second with an average entropy
$1-10^{-5}$.''
 \end{quotation}
\citet{ber14} mentions that this device satisfies all requirements for PTG.3 class random  generation.

\subsection{The Littlewood generator}
The mathematician E.~J. Littlewood suggests a stream cipher based on properties of the logarithm function.
When translated from a 26-ary alphabet to bits, this yields the following production rule:
For a starting value $x$ and fixed $d$, the $i$th bit of the key stream is the $d$th post-decimal bit of $\log_2 (x + i)$. Littlewood writes about the cipher:
\begin{quotation}
 ``The legend that every cipher is breakable is of course absurd, though still widespread among people who should know better.
  I give a sufficient example, without troubling about its precise degree of practicability. [...] It is sufficiently obvious that a \emph{single} message cannot be unscrambled [...]''
\end{quotation}
It has been shown by \citet{wil79} and \citet{ste04a} how Littlewood's cipher can easily be broken. As the output bits of the generator are taken from a curve over the reals, consecutive output bits can be used to recover this curve, 
and therefore the seed that is used to initialize the generator.

Moreover, the statistical properties of the generator can be further compromised by improper parameter choice.
Call $\log_2 (x + i)$ the $x + i$-th \emph{row value}.
When $x + i$ approaches a power of $2$, the row values approach a whole number, and the first post-decimal bits of them will be $1$.
Littlewood addresses this implicitly, by taking the 6th and 7th post-decimal digits of a logarithm with 5 input digits.
In this way, he avoids the first five digits, which predictably approach the value $.99999$.
When the generator is expected to produce a maximum of $N$ output bits, the first $\left\lceil \log_2 (x + N) \right\rceil$ post-decimal bits of the row values have an elevated probability of being $1$.
Thus, we need to have $d$ greater than that in order to not reduce the bit entropy unnecessarily.

\subsection{Linear Congruential generators}
Linear congruential generators as presented in \citet{leh51} produce a sequence of values in $\mathbb{Z}_M$, generated by iteratively applying
\begin{equation*}
  x_i = s \cdot x_{i-1} + t  \text{ in $\mathbb{Z}_M$}
\end{equation*}
to a secret random seed $x_0 \in \mathbb{Z}_M$ provided by an external source. The secret parameters $s$ and $t$ are chosen from $\mathbb{Z}_M$.

While the byte distribution of LCG outputs is generally well-distributed, with byte entropy close to maximal, the generated sequences are predictable and therefore cryptographically insecure. 
%A paper on predicting pseudorandom sequences in general 
The author of the two papers
 \citet{plu82b} and \citet{boy89} describes how to recover the secret $(s, t, M)$ from the sequence of $(x_i)_{i \geq 0}$ alone.
A possible mitigation against this attack is to output only some \emph{least significant bits} of the $x_i$.
% \citet{has85} describes a lattice based attack on truncated LCGs where $(s,t,M)$ are public.
H\aa stad \& Shamir (\citeyear{has85}) describe a lattice based attack on truncated LCGs where $(s,t,M)$ are public.
This attack can be used to predict LCGs that output at least $\frac{1}{3}$ of the bits of the $x_i$.
We are neither aware of a more powerful attack on the LCG nor of a security argument for a certain application.

In our evaluation we varied truncation lengths, without observing significant changes in the statistical features of the output.
As a conservative measure, we decided to publish results for an LCG outputting only the least significant byte of every $x_i$, which seems reasonably secure.

In our implementation, M was to chosen to be the next prime, given a $3000$ bit seed. By doing so, primality testing increases initialization and cold start  times.

\subsection{The Blum-Blum-Shub Generator}\label{gen:bbs}


The Blum-Blum-Shub generator produces a pseudorandom bit sequence from a random seed by repeatedly squaring modulo a so called \textit{Blum integer} $N = p \cdot q$, where $p$ and $q$ are large random primes congruent to $3$ mod $4$.
In each round, the least significant bit of the intermediate result is returned.

\citet{alecho88} proved that factoring the Blum integer $N$ can be reduced to being able to guess the least significant bit of any intermediate square with nonnegligible advantage.
The output of this generator is thus cryptographically secure under the assumption that factoring Blum integers is a hard problem.

\subsection{The RSA generator}
The RSA generator was first presented by \citet{sha83b} and is one of the
PRGs that are proven to be cryptographically secure under certain number-theoretical assumptions.
Analogously to the RSA crypto scheme, the generator is initialized by
choosing a modulus $N$ as the product of two large random primes, and an
exponent $e$ with $1 < e < \varphi(N)-1$ and $\gcd(e, \varphi(N))$. Here,
$\varphi$ denotes Euler's totient function.

Starting from a seed $x_0$ provided by an external source the generator
iteratively computes
\begin{align*}
    x_{i+1} = x_i ^e \mod N,
\end{align*}
extracts the least significant $k$ bits of each intermediate result and
concatenates them as output.

Our implementation uses a random 3000-bit Blum integer (see section \ref{gen:bbs}) as modulus $N$ and various choices for the parameters $e$
and $k$.
 For our tests, we choose $e = 3$, provided that $\gcd(e, \varphi(N)) = 1$, as for small exponents the generator is expected to work fast and because it allows us to compare the results to the runtime of the Blum-Blum-Shub generator.
 % (cf. section \ref{gen:bbs}). 
 Additionally, we test the larger exponent $e = 2^{16} + 1$, which is widely used in practice for it is a prime and its structure allows efficient exponentiation.

\citet{alecho88} it is shown that the RSA generator is pseudorandom for $k \leq \log n$, under the assumption that the RSA inversion problem is hard.  Here, $n$ denotes the bit size of the modulus. Under stronger assumptions regarding the hardness of the RSA inversion problem,  \citet{stepie06} prove the security of the generator for $k \leq n \cdot (\frac{1}{2} - \frac{1}{e} - \varepsilon - o(1))$ for any $\varepsilon > 0$. 
% Thus, corresponding to the size of 3000 bits of our modulus, we also test the choice of $k = 1400$.
%Additionally, we
We test  the two choices of $k = 1400$ and $k=1$, the latter for comparison with the BBS generator.


\subsection{The Nisan-Wigderson generator}
Each output bit of the Nisan-Wigderson PRG is produced by a ``hard'' function $f: \{0,1\}^s \rightarrow \{0,1\}$. 
The arguments of $f$ are chosen sequentially from a design $S = (S_1, S_2, S_3, ..., S_n)$ 
of subsets of $\{1,\dots,k\}$ for some $ k \in \mathbb N$, which can intersect in at most $t$ elements,
so that for all $i \neq j \leq n$ we have
\begin{align*}
%\forall_{i,j \le n,\, i \neq j}:\: 
S_i,S_j \subset \{1, ..., k\},\, s = \#S_i =\#S_j, \#(S_i \cap S_j)  \le t.
\end{align*}
The output $y$ is generated by applying $f$ to arguments derived from a $k$-bit seed $x$.
Each set $S_i = \{S^1_i,\dots S^s_i\}$ ($1 \leq i \leq n$) describes the bits of $x$ at which $f$ is evaluated.
For the $i$th output bit $y^i$ in $y$ we have
\begin{align*}
y^i = f(x|_{S^1_i,\dots,S^s_i}).
\end{align*}
For benchmarking we used an odd $s$ and for $z = (z_1,\ldots,z_s) \in \{0,1\}^s$, $f(z) =$ XOR of the bits of
\begin{align*}
(z_1,\ldots,z_{(s+1)/2})_2 + (z_{(s+1)/2},\dots,z_s)_2 \text{ in } \mathbb Z_{(s+1)/2},
% g(x) = x|_{S^1_i,\dots,S^{(s+1)/2}_i} + x|_{S^{(s+1)/2}_i,\dots,S^s_i} \text{ in } \mathbb Z_{(s+1)/2}
\end{align*}
where $(u)_2$ is the integer with binary representation $u$.
This yields $n$ pseudorandom bits in total. The computation can be done in parallel, since each bit only depends on the elements of $S_i$. 
In order to make the performance of Nisan-Wigderson PRG more stable and predictable we used only a single thread. 
The throughput of Nisan-Wigderson with $s = 131$, $k =17161$ and $n = 2248091$, as in the table, does not account for the initialization time. Note that we used two different seeds for generating $512$ kB of data, since a single seed only provides roughly 281 kB of data.

% The results can be found in the table. 
By pre-processing the design, time for generating the sets is not taken into account.

The generator is known to be pseudorandom if $f$ is ``hard'' in a suitable sense. We do not claim this for our choice of $f$
and thus not the pseudorandomness property.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{stabil.eps}
\caption{Generator throughput after initialization for different output lengths.}
\label{tab:cold}
\end{figure}

\section{Evaluation}\label{s:eval}
To evaluate efficiency as well as statistical properties for the presented generators,
we developed a framework that  runs the software generators based on seed data from the PRG310-4.
To this end, we implemented the generators in C, using the  GMP library, see \citet{gmp14}, to accomplish large integer and floating point arithmetic.
The evaluation framework sequentially runs all generators,  reading from one true random source file of 512 kB and producing 512 kB each, while measuring the runtime of each generator and the byte entropy of each output.

All algorithms were run on a Lenovo ThinkPad T530 with a Intel Core i7-3610QM @ 2.30GHz CPU with 12 GiB RAM. 
We used Linux Mint 17.1 Cinnamon 64-bit, Linux 3.13.0-55-generic, glibc 2.19, libgmp 5.1.3, libmpfr 3.1.2-p3 and gcc 4.8.4.


\subsection{Isolating the generation process}
In an attempt to increase comparability, we decided to split up the initialization and generation processes and measure the time for the generation only after a certain amount of data was generated.
This way, the throughput of the generators had time to stabilize and we thus omit possible noise that is produced when the generator is first started, see
the left part of Figure \ref{tab:cold}).
To determine the appropriate amount of data to be generated before the measurement, we measured throughputs for increasing amounts of data 
so that we could see at which point the throughputs stabilize.
%As the measurements in table \ref{tab:cold} show, all generators stabilize after the generation of at least $1$ MB, such that we used this value as offset from which on to measure the generation time.
%The warm start measurements more closely related to actual use cases, since PRGs output streams of data. %Herbei argumentiert
To compare the two generation variants we have listed throughputs for both, along with byte entropy and normalized throughput of the additional measurements in Table \ref{tab:warm}. For parameter generation, only the fast variant of RSA actually generated a Blum integer. The variants RSA slow, as well as the Blum-Blum-Shub generator then used the computed value, explaining the considerable change for the timings of the slow variant of the RSA generator. Also for the linear congruential generator a prime modulus was generated. Note that Table \ref{tab:results} given in the abstract only lists the results for the generators \emph{with} initialization.

%%%%%%% Warm start measurements %%%%%%%
   \begin{table}[tb]
    \small
      \centering
        \renewcommand{\arraystretch}{1.2}
        \begin{tabular}{l|cccc}
                                           & byte entropy   & throughput             & throughput             & throughput      \\
                                           &                & w/o init               & w/ init                & w/o init        \\
                                           & $[\text{bit}]$ & $[\text{kB}/\text{s}]$ & $[\text{kB}/\text{s}]$ & normalized      \\
        \hline          
        \hline
          PRG310-4, AES post-processing    & \bmAESByteEnt  & ---                    & \bmAESThruAbsW         & \bmAESThruNormW \\
          \texttt{/dev/random}, in the lab & \drLabByteEnt  & ---                    & \drLabThruAbsW         & \drLabThruNormW \\
        \hline
          Littlewood                       & \lwByteEnt     & \lwThruAbs             & \lwThruAbsW            & \lwThruNorm     \\
          Linear congruential generator    & \lcgByteEnt    & \lcgThruAbs            & \lcgThruAbsW           & \lcgThruNorm    \\
        \hline
          Blum-Blum-Shub                   & \bbsByteEnt    & \bbsThruAbs            & \bbsThruAbsW           & \bbsThruNorm    \\
          RSA, fast                        & \rsafByteEnt   & \rsafThruAbs           & \rsafThruAbsW          & \rsafThruNorm   \\
          \quad slow                       & \rsaByteEnt    & \rsaThruAbs            & \rsaThruAbsW           & \rsaThruNorm    \\
          Nisan-Wigderson                  & \nwByteEnt     & \nwThruAbs             & \nwThruAbsW            & \nwThruNorm     \\
        \end{tabular}
        \caption{\small Overview of the results for warm-starting generators (w/o init). Cold-start (w/ init) throughput as comparison.} \label{tab:warm}
    \end{table}

\subsection{Applying the NIST test suite}
The NIST test suite, see \citet{sts10}, is a collection of statistical tests, aimed at checking blocks of pseudorandomly generated data for statistical irregularities. 
The user specifies the tests to be run on the data, as well as the parameters of the tests and the block size used for processing the data. 
The output is an enumeration of the applied tests, the number of blocks for which each 
test succeeded and a $p$-value, providing a confidence level for the correctness of the test results. 
For each test, the NIST specification gives a minimum $p$-value that indicates whether the input data should be regarded as random.

For data to be considered statistically random, a minimal number of blocks must pass various tests successfully. 
Each test has its own minimal block size. For instance, Maurer's universal test, see \citet{mau92}, requires blocks to be roughly 300 kB at least.

With suitable parameters, all but the Littlewood generator did pass all tests. As the byte entropy of the outputs of the Littlewood generator already indicated statistical weaknesses, this does not come as a surprise.
The fact that the cryptographically insecure linear congruential generator passes these tests with flying colors substantiates the well-known opinion that such tests are of little cryptographic significance.

\section{Conclusion}\label{s:conc}
We implemented a number of software random generators and compared their performance to physical generators. \texttt{/dev/random} 
is way too slow to be of practical use. The generator PRG310-4 is roughly as good as our Blum-Blum-Shub implementation. However, both are surpassed by the 
RSA generator when run with a fast parameter set, which offers the same level of  security. 

The most interesting --- and surprising to us --- result is that number-theoretic methods outperform hardware-based approaches by far. Their additional advantage is security under standard complexity assumptions such as the hardness of factoring certain integers. However, they still need random seeds. This suggests a profitable symbiosis of hardware-generated seeds and number-theoretic high throughput --- rather the reverse of the situation in other cryptographic contexts, say, the Diffie-Hellman exchange of keys for fast AES encryption.

\section{Future work}\label{s:fw}
We decided on parameter choices that look reasonable to us, but many alternatives are possible. Do other parameter settings lead to qualitatively different results?

Practical use of our findings has not taken place yet. Depletion of \texttt{/dev/random} is a realistic issue --- 
workarounds for implied problems even suggest using \texttt{/dev/urandom} as a physical generator, see \citet{sea08}. 
However, prohibiting the use of \texttt{/dev/urandom} for key generation is under debate, see \citet{bern14}. From the BSI's point of view, 
\texttt{/dev/urandom} fulfills all requirements for NTG.1 as defined in \citet{bsi11}, except for NTG.1.1, see \citet{bsi-linux-rng}.

Given benchmarking results and consumed entropy, using \texttt{/dev/random} for seed generation and then running a secure software PRG looks promising.
Kernel based implementations of the algorithms we investigated are not available as of today. Benchmarking and field testing are yet to be done.
As a next step, implementing and testing on kernel level using optimized implementations is recommended.
% Bibliography

\section*{References}
\vspace{-1cm}
\ifrefsbib
\bibliography{journals,refs,lncs,addition}
\else
\input{kryptotag_paper03.bbl}
\fi

\end{document}
