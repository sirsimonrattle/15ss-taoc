#!/usr/bin/python
from __future__ import with_statement
from csv import DictReader

othergens = {
    'drField': {
        'ByteEnt': 7.99979,
        'Runtime': 9.169e10,
        'ThruAbs': 5.58396e-3
    },
    'drLab': {
        'ByteEnt': 7.99948,
        'Runtime': 2.671e12,
        'ThruAbs': 1.91672e-4
    },
    'bmPlain': {
        'ByteEnt': 7.99963,
        'Runtime': 16308400
    },
    'bmAES': {
        'ByteEnt': 7.99963,
        'Runtime': 36524300
    }
}


def pretty_float(f):
    mant, exp = '{:e}'.format(f).split('e')
    return '\\({:2.3f} \\times 10^{{{:d}}}\\)'.format(float(mant), int(exp))


def print_macro(gen, metric, value):
    print '\\newcommand\\{}{}{{{}}}'.format(gen, metric, value)

with open('main_results.csv', 'r') as f:
    for row in DictReader(f):
        print_macro(row['gen'], row['metric'], row['value'])

minThruAbs = 0
with open('main_results_incl_init.csv', 'r') as f:
    for row in DictReader(f):
        print_macro(row['gen'], row['metric'] + 'W', row['value'])
        if row['gen'] == 'rsa' and row['metric'] == 'ThruAbs':
            minThruAbs = float(row['value'])

for gen, values in othergens.items():
    print_macro(gen, 'ByteEnt', values['ByteEnt'])
for gen in ['bmPlain', 'bmAES']:
    values = othergens[gen]
    print_macro(gen, 'RuntimeW', values['Runtime'])
for gen in ['drField', 'drLab']:
    values = othergens[gen]
    print_macro(gen, 'RuntimeW', pretty_float(values['Runtime']))
    print_macro(gen, 'ThruAbsW', pretty_float(values['ThruAbs']))
    print_macro(gen, 'ThruNormW', pretty_float(values['ThruAbs'] / minThruAbs))
