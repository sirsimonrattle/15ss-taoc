\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Motivation}{4}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Hardware generators}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Software generators}{8}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Linear Congruential Generator}{9}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{RSA, BBS}{10}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{3}{Other generators}{11}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Results}{12}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Conclusion \& Future Work}{14}{0}{5}
