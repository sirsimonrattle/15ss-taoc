\documentclass[xcolor=pst, dvips, t]{beamer}
\usepackage{etex}
\usetheme[logo=true]{cosecstudents}
\usepackage[german,english]{babel}
\usepackage[backend=bibtex, style=alphabetic, natbib=true]{biblatex}
\addbibresource{journals.bib}
\addbibresource{refs.bib}
\addbibresource{lncs.bib}
\addbibresource{addition.bib}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{calc}

%\setbeamertemplate{section in toc}[sections numbered]
%\setbeamertemplate{subsection in toc}[subsections numbered]
\setbeamerfont{block body}{size=\normalsize}
\renewcommand*{\bibfont}{\tiny}
\providecommand\Hide[1]{}

%% 
%% This file makes use of pstricks (via the logo option of the cosecstudents theme).
%% It must be compiled with latex, bibtex, latex (x2), dvips and ps2pdf.
%%


\title{Comparative Analysis of Pseudorandom Generators}
%\subtitle{Subtitle}

\author{Aleksei~Burlakov, %
  \underline{Johannes~vom~Dorp}, %
  Joachim~von~zur~Gathen, %
  Sarah~Hillmann, %
  Michael~Link, %
  Daniel~Loebenberger, %
  \underline{Jan~L\"uhr}, %
  \underline{Simon~Schneider }\& %
  Sven~Zemanek}
\institute{Bonn-Aachen International Center for Information Technology}
\date{9th July, 2015}

\makeatletter

\begin{document}

\begin{frame}
  \titlepage 
  \logo{\coseclogoW{4cm}}
  \begin{center}
    \insertlogo
  \end{center}
\end{frame}

\begin{frame}\frametitle{Introducing ourselves}
  \begin{center}
    \includegraphics[scale=1]{img/submission.eps}

  Course: ``The art of cryptography: Heads and tails - Cryptographic random generation'', B-IT, Summer 2015

  \end{center} 
\end{frame}

\begin{frame}\frametitle{Table of contents}

  \vspace{30 px}

  \tableofcontents[hideallsubsections]
\end{frame}

\section{Motivation}

\begin{frame}\frametitle{Motivation}
  \begin{center}
    \includegraphics[scale=.6]{img/random_number-converted-to.eps} \\
    \tiny Source: \url{https://xkcd.com/221/}, CC-BY-NC 2.5
  \end{center} 
  
  \onslide*<2->{
  We need random numbers with high speed and in good quality
  
  \begin{itemize}
   \item Problem: Which generators to use? Security? Throughput?
   \item Our approach: Implement and benchmark suitable PRNG algorithms on state-of-the-art hardware (Lenovo ThinkPad T530 with a Intel Core i7-3610QM @ 2.30GHz CPU).
  \end{itemize}
  }
\end{frame}

\section{Hardware generators}

\begin{frame}
\frametitle{Table of contents}

  \vspace{30 px}

  \tableofcontents[current, hideallsubsections]

\end{frame}

\begin{frame}\frametitle{Getting entropy}
  \begin{columns}
  
  \column{0.5\linewidth}
  \begin{block}{Linux' \texttt{/dev/random}}
  \begin{minipage}[t][10\baselineskip]{\linewidth}
    Gathers entropy from system events:

	\vspace{10 px}
 
    \begin{itemize}
      \item User: keyboard/mouse
      \item Disk timing
      \item Interrupts
      \item External sources % todo: improve wording
    \end{itemize}
    
    \vfill
    Fulfills BSI class NTG.1    
  \end{minipage}
  \end{block}
  
  \column{0.5\linewidth}
  \begin{block}{PRG310-4}
  \begin{minipage}[t][10\baselineskip]{\linewidth}
    System of noisy diodes
    
    Average entropy $1-10^{-5}$
    
    \vspace{1cm}
    \includegraphics[scale=.75]{img/i1520628-1-converted-to.eps}%\footnotemark
    
    \vfill
    Fulfills BSI class PTG.3 
  \end{minipage}
  \end{block}
  
  \end{columns}

  \vspace{1cm}

  \begin{center}
	\small 
	Details: \cite{lac12,bsi-linux-rng,bsi-rng-classes,ber14,schkil03}
  \end{center}

\end{frame}

\section{Software generators}

\begin{frame}
\frametitle{Table of contents}

  \vspace{30 px}

  \tableofcontents[current, hideallsubsections]

\end{frame}

\subsection{Linear Congruential Generator}
\begin{frame}\frametitle{Linear Congruential Generator (LCG)}
  Secret: seed $x_0$ \quad
  Parameters: $s, t$ integer, $M$ prime \\
  Generate: $x_{i+1} = sx_i + t \pmod M$
  
  \begin{examples}\vspace{-.3cm}
    Let: $M = 5,\, x_0 = 3,\, s = 2,\, t = 1 \Rightarrow x_{i+1} = 2 \cdot x_i + 1 \pmod 5$
    
    $x_0 = 3, x_1 = 2, x_2 = 0, \dots$
  \end{examples}
  
  Attacks: \cite{boy89, plu82b, has85}. Truncation avoids illustrated flaws. \vspace{.3cm}

  \begin{tabular}{cc}
    \includegraphics[scale=.5]{img/distrib_lcg-converted-to.eps} 
    &
    \includegraphics[scale=.5]{img/distrib_lcg_notprime-converted-to.eps} 
    \\
    
    {\small byte distribution for prime $M$}
    &
    {\small byte distribution for nonprime $M$}
    \\
  \end{tabular}
  \vspace{-.2cm}

  
  \begin{block}{Parameters}\vspace{-.3cm}
  \begin{itemize}
    \item $x_0, s, t$: $3000$ bit random number, $M = $ \texttt{next\_prime}$(2^{3000})$
    \item Truncation: least significant byte
  \end{itemize}
  \end{block}
\end{frame}

\subsection{RSA, BBS}
\begin{frame}\frametitle{RSA, Blum-Blum-Shub}
  Secret: seed $x_0$ \quad
  Parameters: $p, q$ prime, $e$ integer \\
  Generate: $x_{i+1} = x_i^e \pmod {N = pq}$
  
    
\begin{block}{RSA}
  $1 < e < \varphi(N),\, \gcd(e, \varphi(N)) = 1$ 
  
\end{block}

\begin{block}{Blum-Blum-Shub (BBS)}
  $p \equiv q \equiv 3 \pmod 4$, $e = 2$ 
\end{block}

\begin{block}{Parameters}
\begin{itemize}
 \item $N$ $3000$-bit random number
 \item $e \in \{ 3, 2^{16} + 1 \}$ (RSA)
 \item Truncating to $k$ bits: $k = 1$ (BBS), $k \in \{ 1, 1400 \}$ (RSA)
 \end{itemize}
 
\end{block}
  \vfill
  \begin{footnotesize}
    Security: \cite{alecho88,stepie06,blublu86}  
  \end{footnotesize}
  
\end{frame}


\subsection{Other generators}
\begin{frame}\frametitle{Other generators}
  \begin{block}{Nisan-Wigderson}
	\begin{itemize}
	\item Generator based on the evaluation of a hard function
	\item Provable security
	\item Quite impractical
	\end{itemize}
  \end{block}
  
  \begin{block}{Littlewood}
	\begin{itemize}
	\item Early example of stream cipher
	\item Based on properties of the log function
	\item Predictable, therefore cryptographically worthless.
	\end{itemize}
  \end{block}
\end{frame}

\include{main_results}

\section{Results}

\begin{frame}
\frametitle{Table of contents}

  \vspace{30 px}

  \tableofcontents[current, hideallsubsections]

\end{frame}

\begin{frame}\frametitle{Benchmarking results} 
\begin{footnotesize}Generating $500$ KiB on a ThinkPad T530 (Intel Core i7-3610QM @ 2.30GHz)\end{footnotesize}
  \begin{table}[h]
    \scriptsize
    \centering
    \renewcommand{\arraystretch}{1.2}
    \begin{tabular}{l|ccc}
                                            & byte entropy    & throughput              & throughput        \\
                                            & $[\text{bit}]$  & $[\text{kB}/\text{s}]$  & normalized        \\
      \hline          
      \hline
        PRG310-4, no post-processing        & \bmPlainByteEnt & \bmPlainThruAbsW         & \bmPlainThruNormW  \\
        \quad AES post-processing           & \bmAESByteEnt   & \bmAESThruAbsW           & \bmAESThruNormW    \\
        \texttt{/dev/random}, in the field  & \drFieldByteEnt & \drFieldThruAbsW         & \drFieldThruNormW  \\
        \quad in the lab                    & \drLabByteEnt   & \drLabThruAbsW           & \drLabThruNormW    \\
      \hline
        Littlewood                          & \lwByteEnt      & \lwThruAbs              & \lwThruNorm       \\
        Linear congruential generator       & \lcgByteEnt     & \lcgThruAbs             & \lcgThruNorm      \\
      \hline
        Blum-Blum-Shub                      & \bbsByteEnt     & \bbsThruAbs             & \bbsThruNorm      \\
        \textbf{RSA}, $e=2^{16}\!\!+\!1$, $1400$ bit/round & \textbf \rsafByteEnt & \textbf \rsafThruAbs        \textbf    & \textbf \rsafThruNorm     \\
        \quad $e=3$, $1$ bit/round          & \rsaByteEnt     & \rsaThruAbs             & \rsaThruNorm      \\
        Nisan-Wigderson                     & \nwByteEnt      & \nwThruAbs              & \nwThruNorm       \\
   \end{tabular}

    %    \begin{tabular}{l|cccc}
                                            %& byte entropy    & runtime           & throughput              & throughput        \\
      %                                      & $[\text{bit}]$  & $[\mu \text{s}]$  & $[\text{kB}/\text{s}]$  & normalized        \\
      %\hline          
      %\hline
      %  PRG310-4, no post-processing        & \bmPlainByteEnt & \bmPlainRuntimeW   & \bmPlainThruAbsW         & \bmPlainThruNormW  \\
      %  \quad AES post-processing           & \bmAESByteEnt   & \bmAESRuntimeW     & \bmAESThruAbsW           & \bmAESThruNormW    \\
      %  \texttt{/dev/random}, in the field  & \drFieldByteEnt & \drFieldRuntimeW   & \drFieldThruAbsW         & \drFieldThruNormW  \\
      %  \quad in the lab                    & \drLabByteEnt   & \drLabRuntimeW     & \drLabThruAbsW           & \drLabThruNormW    \\
      %\hline
      %  Littlewood                          & \lwByteEnt      & \lwRuntime        & \lwThruAbs              & \lwThruNorm       \\
      %  Linear congruential generator       & \lcgByteEnt     & \lcgRuntime       & \lcgThruAbs             & \lcgThruNorm      \\
      %\hline
      %  Blum-Blum-Shub                      & \bbsByteEnt     & \bbsRuntime       & \bbsThruAbs             & \bbsThruNorm      \\
      %  RSA, $e=2^{16}\!\!+\!1$, $1400$ bit/round & \rsafByteEnt    & \rsafRuntime      & \rsafThruAbs            & \rsafThruNorm     \\
      %  \quad $e=3$, $1$ bit/round          & \rsaByteEnt     & \rsaRuntime       & \rsaThruAbs             & \rsaThruNorm      \\
      %  Nisan-Wigderson                     & \nwByteEnt      & \nwRuntime        & \nwThruAbs              & \nwThruNorm       \\
%   \end{tabular}
  \end{table}
  
  \begin{tikzpicture}[yscale = .25]
    \draw (-6, 0) to (4, 0);
    \foreach \x in {-5, ..., 3} {
      \node at (\x, 0) {\tiny $\mid$};
      \node at (\x, -1) {\tiny $10^{\x}$};
    }
    
    %% x values are log_10 of \genThruNorm
    \foreach \pos/\level/\label in {%
      -4.57675/2/{\texttt{/dev/random}, lab}, %
      -3.11182/1/{\texttt{/dev/random}, field}, %
       0.00000/1/{RSA, slow}, %
       0.28780/5/{PRG310-4, w/ AES}, %
       0.59770/2/{BBS}, %
       0.63749/4/{PRG310-4, raw}, %
       0.66370/3/{Littlewood}, %
       1.40943/2/{Nisan-Wigderson}, %
       1.42341/1/{LCG}, %
       2.41823/1/{RSA, fast} %       
      } {
      \draw ($(\pos, \level)+(.2, 0)$) -- (\pos, \level) -- (\pos, 0);
      \fill [yscale = 4] (\pos, 0) circle[radius = 2pt];
    }
    \foreach \pos/\level/\label in {%
      -4.57675/2/{\texttt{/dev/random}, lab}, %
      -3.11182/1/{\texttt{/dev/random}, field}, %
       0.00000/1/{RSA, slow}, %
       0.28780/5/{PRG310-4, w/ AES}, %
       0.59770/2/{BBS}, %
       0.63749/4/{PRG310-4, raw}, %
       0.66370/3/{Littlewood}, %
       1.40943/2/{Nisan-Wigderson}, %
       1.42341/1/{LCG}, %
       2.41823/1/{RSA, fast} %       
      } {
      \node [anchor = west, fill = white!30, inner sep = 0] at ($(\pos, \level) + (.1, 0)$) {\tiny \label};
    }
  \end{tikzpicture}
\end{frame}

\section{Conclusion \& Future Work}

\begin{frame}
\frametitle{Table of contents}

  \vspace{30 px}

  \tableofcontents[current, hideallsubsections]

\end{frame}

\begin{frame}\frametitle{Conclusion \& Future Work}
  \begin{block}{Main Results}
    \begin{itemize}
      \item Physical random generation is slow, a secure PRNG is just as good and \emph{faster}.
      \item RSA looks most promising, with $\rsafThruAbs$ kB/s.
      \item All but the Littlewood pass NIST-Test Suite
    \end{itemize}
  \end{block}  
  
  \begin{block}{Next steps}
    \begin{itemize}
      \item Optimize the code
      \item Implement blocking mode
      \item Implement Kernel module
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[c]
\frametitle{Discussion}
\Large
\begin{center}
  Any further questions?

  \vspace{1cm}  
  Thank you for your attention!
\end{center}
\end{frame}



%\appendix
\section*{Extra slides}
\begin{frame}[noframenumbering]\frametitle{The Nisan-Wigderson Construction}
  \small
  Need hard\footnote{Close to maximum complexity of a Boolean circuit, but ``fast'' in evaluating $y^{(1)}\dots y^{(n)}$.There is no known example of such a function yet.} function $f \colon \mathbb B^s \to \mathbb B$.
  Arguments for $f$ are determined by a \emph{design} $(S_1,\dots, S_n)$ of sequences sharing at most $t$ elements. $\forall_{i,j \le n,\, i \neq j}$:
  \begin{itemize}
    \item $S_i\subset \{ 1, \dots, k \}$ using canonical ordering.
    \item $\#S_i = s$
    \item $\#(S_i \cap S_j)  \le t$
  \end{itemize}
  
  Bitwise generation with seed $x$: $y^{(i)} = f( x^{(S_{i,1})}, \dots, x^{(S_{i,s})} )$ 
  
  \begin{block}{Parameters}
   \begin{enumerate}
      \item $t=2,\, s=131,\, n=2248091$, $k=17161$, $S$ preprocessed
      \item $ f := parity(g)$
      \item $g(x) = x|_{S_{i,1},\dots,S_{i,66}} + x|_{S_{i,66},\dots,S_{i,131}} \pmod{ 2^{66}}$
      \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}[noframenumbering] \frametitle{Littlewood's Generator}
  \alert{Do not use}\\
  Idea: Numbers in a table of logarithms look well mixed.\\
  
  Secret: seed $x$ \quad
  Parameter: $d$ integer \\
  Generate: $x_i =$ $d$-th post-decimal bit of $\log_2(x + i)$
  
  \citet{wil79} and \citet{ste04a} demonstrate how $d$ and $x$ can be recovered from a sequence of $x_i$ using lattices.
  
  Besides, when $d$ is too close to the decimal point, $x_i$ are biased towards $1$.   

  \begin{block}{Parameters} \vspace{-.3cm}
    \begin{itemize}
     \item $x$ is a $5$ bit random number
     \item $d=29$
    \end{itemize}  
  \end{block}
  \vspace{-.3cm}
  
  \begin{center}
    \includegraphics[scale=.20]{img/distrib_lw-converted-to.eps} \\
    \tiny biased output results in reduced entropy
  \end{center}
  
\end{frame}

\end{document}