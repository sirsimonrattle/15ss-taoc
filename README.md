Heads and tails 2015: exercise sheet #6
---------------------------------------
Feel free to break everything, that's what version control is for.
There are two main subfolders here:

 * `code` should contain the implementations of the different pseudorandom generators, as well as the code to run them.
 * `documentation` is meant to contain the texts that we write about the experiment.

## Build instructions
1. `cmake .` To generate the Makefile(s)
2. `make <target>`, where `<target>` can be:
    * *none*, to build all executables, including `./main`
    * `test`, to build and run the tests
    * `pdf`, to build the documentation

## Note to benchmarking
1. Change BYTES_TO_GENERATE in generators.h accordingly.
2. run ./main --csv 20 times and pipe output to file
3. Name file as needed for scripts in documentation/plots
4. Run scripts
