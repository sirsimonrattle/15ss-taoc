#ifndef EXEC_GENS_H
#define EXEC_GENS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <errno.h>
#include "generators.h"
#include "util.h"

enum gen_algorithm {
	LCG, BBS, RSAGEN, NW, LITTLEWOOD, AES, AESNI, AESOPENSSL
};
#define ALGO_NO_MATCH -1
enum gen_algorithm repl_algo_from_str(const char *str);
const char *repl_str_from_algo(enum gen_algorithm algo);

// Definition of REPL_ERROR_T is in generators.h
REPL_ERROR_T repl_exec_line(char *line);

struct generator {
	enum gen_algorithm algo;
	GEN_ERROR_T (*init)(struct seed_file_state_struct *seed_file,
			uint32_t num_bytes);
	GEN_ERROR_T (*generate)(uint8_t *outbuffer,
			struct seed_file_state_struct *seed_file, uint32_t num_bytes);
	GEN_ERROR_T (*free)(void);
	REPL_ERROR_T (*set_parameter)(const char *key, const char *value);
	REPL_ERROR_T (*load_default_parameters)(void);
	REPL_ERROR_T (*show_parameters)(const char *experiment_name,
			FILE *msg_stream);
};

#endif /* ifndef EXEC_GENS_H */
