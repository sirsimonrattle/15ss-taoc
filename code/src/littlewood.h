#ifndef LITTLEWOOD_H
#define LITTLEWOOD_H

#include <gmp.h>
#include <mpfr.h>
#include "util.h"
#include "generators.h"

/*
 * Initializes the generator, using the specified bit string as the initial value x.
 *
 * Parameters:
 * x_bits    NULL-terminated string of {0,1}, giving the seed of the generator
 * num_bytes How many bytes the generator should produce
*/
GEN_ERROR_T littlewood_str_init(char * x_bits, uint32_t num_bytes);

/*
  Prints the generator's parameters to STDOUT.
*/
void littlewood_print_info();


int littlewood_single_bit(void);

DECLARE_GEN_REPL_FUNCTIONS(littlewood)

#endif
