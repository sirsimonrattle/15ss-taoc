#include "repl.h"
#include "lcg.h"
#include "bbs.h"
#include "rsa.h"
#include "nisan_wigderson.h"
#include "littlewood.h"
#include "aes.h"
#include "aesni.h"
#include "aes_openssl.h"

char experiment_name[256];
enum gen_algorithm algo;
int how_many_bytes;
struct seed_file_state_struct *seed_file_state;
uint8_t result_buffer[BYTES_TO_GENERATE+10000];
struct generator *current_generator;

struct generator GEN_LCG = {
	.algo = LCG,
	.init = &lcg_init,
	.generate = &lcg_generate,
	.free = &lcg_free,
	.set_parameter = &lcg_set_parameter,
	.load_default_parameters = &lcg_load_default_parameters,
	.show_parameters = &lcg_show_parameters,
};
struct generator GEN_BBS = {
	.algo = BBS,
	.init = &bbs_init,
	.generate = &bbs_generate,
	.free = &bbs_free,
	.set_parameter = &bbs_set_parameter,
	.load_default_parameters = &bbs_load_default_parameters,
	.show_parameters = &bbs_show_parameters,
};
struct generator GEN_RSA = {
	.algo = RSAGEN,
	.init = &rsa_init,
	.generate = &rsa_generate,
	.free = &rsa_free,
	.set_parameter = &rsa_set_parameter,
	.load_default_parameters = &rsa_load_default_parameters,
	.show_parameters = &rsa_show_parameters,
};
struct generator GEN_NW = {
	.algo = NW,
	.init = &nw_init,
	.generate = &nw_generate,
	.free = &nw_free,
	.set_parameter = &nw_set_parameter,
	.load_default_parameters = &nw_load_default_parameters,
	.show_parameters = &nw_show_parameters,
};
struct generator GEN_LITTLEWOOD = {
	.algo = LITTLEWOOD,
	.init = &littlewood_init,
	.generate = &littlewood_generate,
	.free = &littlewood_free,
	.set_parameter = &littlewood_set_parameter,
	.load_default_parameters = &littlewood_load_default_parameters,
	.show_parameters = &littlewood_show_parameters,
};
struct generator GEN_AES = {
	.algo = AES,
	.init = &aes_init,
	.generate = &aes_generate,
	.free = &aes_free,
	.set_parameter = &aes_set_parameter,
	.load_default_parameters = &aes_load_default_parameters,
	.show_parameters = &aes_show_parameters,
};
struct generator GEN_AESNI = {
	.algo = AESNI,
	.init = &aesni_init,
	.generate = &aesni_generate,
	.free = &aesni_free,
	.set_parameter = &aesni_set_parameter,
	.load_default_parameters = &aesni_load_default_parameters,
	.show_parameters = &aesni_show_parameters,
};
struct generator GEN_AESOPENSSL = {
		.algo = AESOPENSSL,
		.init = &aes_openssl_init,
		.generate = &aes_openssl_generate,
		.free = &aes_openssl_free,
		.set_parameter = &aes_openssl_set_parameter,
		.load_default_parameters = &aes_openssl_load_default_parameters,
		.show_parameters = &aes_openssl_show_parameters,
};

/*
 * Approximates Java's Enum.valueOf(String)
 *
 * @returns EXIT_FAILURE in case of mismatch
 */
enum gen_algorithm repl_algo_from_str(const char *str) {
	if (!strcasecmp(str, "LCG"))
		return LCG;
	if (!strcasecmp(str, "BBS"))
		return BBS;
	if (!strcasecmp(str, "RSA"))
		return RSAGEN;
	if (!strcasecmp(str, "NW"))
		return NW;
	if (!strcasecmp(str, "LITTLEWOOD"))
		return LITTLEWOOD;
	if (!strcasecmp(str, "AES"))
		return AES;
	if (!strcasecmp(str, "AESNI"))
		return AESNI;
	if (!strcasecmp(str, "AESOPENSSL"))
		return AESOPENSSL;

	return ALGO_NO_MATCH;
}

const char * repl_algo_from_enum(const enum gen_algorithm algo) {
	switch (algo) {
	case LCG:
		return "LCG";
	case BBS:
		return "BBS";
	case RSAGEN:
		return "RSA";
	case NW:
		return "NW";
	case LITTLEWOOD:
		return "LITTLEWOOD";
	case AES:
		return "AES";
	case AESNI:
		return "AESNI";
	case AESOPENSSL:
		return "AESOPENSSL";
	}

	return NULL;
}

REPL_ERROR_T repl_set_name(const char *name);
REPL_ERROR_T repl_use_generator(const char *algo);
REPL_ERROR_T repl_set_parameter(const char *key, const char *value);
REPL_ERROR_T repl_show_parameters(void);
REPL_ERROR_T repl_generate_bytes(int how_many);
REPL_ERROR_T repl_dump_bytes(const char *fname);

REPL_ERROR_T repl_exec_line(char *line) {
	char *cmd, *arg1, *arg2;

	line[strlen(line) - 1] = '\0';
	while (isspace(*line))
		line++;

	if ('#' == line[0] || 0 == strlen(line))
		return REPL_SUCCESS;

	cmd = strtok(line, " ");
	arg1 = strtok(NULL, " ");
	arg2 = strtok(NULL, " ");

	if (!strcasecmp("set_name", cmd)) {
		return repl_set_name(arg1);
	} else if (!strcasecmp("use_generator", cmd)) {
		return repl_use_generator(arg1);
	} else if (!strcasecmp("set_parameter", cmd)) {
		return repl_set_parameter(arg1, arg2);
	} else if (!strcasecmp("show_parameters", cmd)) {
		return repl_show_parameters();
	} else if (!strcasecmp("generate_bytes", cmd)) {
		return repl_generate_bytes(atoi(arg1));
	} else if (!strcasecmp("dump_bytes", cmd)) {
		return repl_dump_bytes(arg1);
	} else {
		fprintf(stderr, "unknown command: %s\n", cmd);
		return REPL_ERROR;
	}
}

/*
 * set_name (name)
 *
 * sets the current experiment's name, i.e. the value of the first column in the csv output.
 */
REPL_ERROR_T repl_set_name(const char *name) {
	if (NULL == name) {
		fprintf(stderr, "set_name needs 1 argument.\n");
		return REPL_ERROR;
	}

	strncpy(experiment_name, name, sizeof(experiment_name));
	return REPL_SUCCESS;
}

/*
 * use_generator (algo)
 *
 * Which generator to use in the experiment.
 * Loads the generator's default configuration.
 */
REPL_ERROR_T repl_use_generator(const char *algo_str) {
	if (NULL == algo_str) {
		fprintf(stderr, "use_generator needs 1 argument.\n");
		return REPL_ERROR;
	}

	algo = repl_algo_from_str(algo_str);
	if (ALGO_NO_MATCH == algo) {
		fprintf(stderr, "unknown algorithm: %s\n", algo_str);
		return REPL_INVALID_KEY;
	}
	switch (algo) {
	case LCG:
		current_generator = &GEN_LCG;
		break;
	case BBS:
		current_generator = &GEN_BBS;
		break;
	case RSAGEN:
		current_generator = &GEN_RSA;
		break;
	case NW:
		current_generator = &GEN_NW;
		break;
	case LITTLEWOOD:
		current_generator = &GEN_LITTLEWOOD;
		break;
	case AES:
		current_generator = &GEN_AES;
		break;
	case AESNI:
		current_generator = &GEN_AESNI;
		break;
	case AESOPENSSL:
		current_generator = &GEN_AESOPENSSL;
		break;
	}

	current_generator->load_default_parameters();

	return REPL_SUCCESS;
}

/*
 * set_parameter (key) (value)
 *
 * Override a part of the current generator's configuration.
 */
REPL_ERROR_T repl_set_parameter(const char *key, const char *value) {
	REPL_ERROR_T err_code;

	err_code = current_generator->set_parameter(key, value);

	switch (err_code) {
	case REPL_SUCCESS:
		break;
	case REPL_INVALID_KEY:
		fprintf(stderr, "Unknown parameter for %s: %s\n",
				repl_algo_from_enum(algo), key);
		break;
	case REPL_INVALID_VALUE:
		fprintf(stderr, "Invalid value for parameter %s.%s: %s\n",
				repl_algo_from_enum(algo), key, value);
		break;
	case REPL_ERROR:
	default:
		fprintf(stderr, "Unknown error during set_parameter: %d\n", err_code);
		break;
	}

	return err_code;
}

/*
 * show_parameters
 *
 * print the generator's configuration in triple format to stdout
 */
REPL_ERROR_T repl_show_parameters() {
	current_generator->show_parameters(experiment_name, stdout);

	return REPL_SUCCESS;
}

// Extract duration in µs out out two timevals
static uint64_t duration_in_micro_s(struct timeval *begin, struct timeval *end) {
	uint64_t begin_s = (begin->tv_sec * 1000000) + begin->tv_usec;
	uint64_t end_s = (end->tv_sec * 1000000) + end->tv_usec;
	return end_s - begin_s;
}

static double throughput_in_kB_s(uint64_t duration, int how_many_bytes) {
	return how_many_bytes * 1000 / ((double) duration);
}

/*
 * generate_bytes (how_many)
 *
 * Run the selected generator to generate some bytes.
 * As series of measurement is then printed to stdout.
 * Each data point is printed as: experiment_name,measurement,value
 *
 * used_seed_init, used_seed_run:
 *     How many seed bytes were consumed during init and run of the generator.
 * duration_init, duration_run:
 *     How long (in microseconds) each of those steps took.
 * throughput_run, throughput_all:
 *     Throughput (in kB/s) of the generator, during just the run, and including the init phase.
 * byte_entropy:
 *     Byte entropy of the generated data.
 */
REPL_ERROR_T repl_generate_bytes(int how_many) {
	struct timeval begin, end;
	GEN_ERROR_T gen_error;
	int used_seed_init, used_seed_run;
	uint64_t duration_init, duration_run;
	double throughput_run, throughput_all, byte_entropy;

	if (0 >= how_many) {
		fprintf(stderr, "generate_bytes needs 1 argument.\n");
		return REPL_ERROR;
	}
	if (BYTES_TO_GENERATE < how_many) {
		fprintf(stderr, "cannot generate more than %d bytes.\n",
		BYTES_TO_GENERATE);
		return REPL_ERROR;
	}
	how_many_bytes = how_many;

	// init generator
	used_seed_init = seed_file_state->bits_read;
	gettimeofday(&begin, NULL);
	gen_error = current_generator->init(seed_file_state, how_many);
	gettimeofday(&end, NULL);
	used_seed_init = seed_file_state->bits_read - used_seed_init;
	duration_init = duration_in_micro_s(&begin, &end);

	// print measurements so far
	if (GEN_SUCCESS != gen_error) {
		fprintf(stderr, "generator init returned nonzero: %d\n", gen_error);
		return REPL_GEN_ERROR_INIT;
	}
	fprintf(stdout, "%s,used_seed_init,%d\n", experiment_name, used_seed_init);
	fprintf(stdout, "%s,duration_init,%ld\n", experiment_name, duration_init);

	// run generator
	memset(result_buffer, 0, how_many);
	used_seed_run = seed_file_state->bits_read;
	gettimeofday(&begin, NULL);
	gen_error = current_generator->generate(result_buffer, seed_file_state, how_many);
	gettimeofday(&end, NULL);
	used_seed_run = seed_file_state->bits_read - used_seed_run;
	duration_run = duration_in_micro_s(&begin, &end);

	// print measurements so far
	if (GEN_SUCCESS != gen_error) {
		fprintf(stderr, "generator run returned nonzero: %d\n", gen_error);
		return REPL_GEN_ERROR_RUN;
	}
	fprintf(stdout, "%s,used_seed_run,%d\n", experiment_name, used_seed_run);
	fprintf(stdout, "%s,duration_run,%ld\n", experiment_name, duration_run);

	// further statistics
	throughput_run = throughput_in_kB_s(duration_run, how_many);
	throughput_all = throughput_in_kB_s(duration_init + duration_run, how_many);
	byte_entropy = get_byte_entropy(result_buffer, how_many);
	fprintf(stdout, "%s,throughput_run,%2.5f\n", experiment_name,
			throughput_run);
	fprintf(stdout, "%s,throughput_all,%2.5f\n", experiment_name,
			throughput_all);
	fprintf(stdout, "%s,byte_entropy,%2.5f\n", experiment_name, byte_entropy);

	// free generator
	gen_error = current_generator->free();

	if (GEN_SUCCESS != gen_error) {
		fprintf(stderr, "generator free returned nonzero: %d\n", gen_error);
		return REPL_GEN_ERROR_FREE;
	}

	return REPL_SUCCESS;
}

/*
 * dump_bytes (fname)
 *
 * Dump the generated data into the named file for external analysis.
 */
REPL_ERROR_T repl_dump_bytes(const char *fname) {
	FILE *fp;
	int fsize;

	fp = fopen(fname, "wb");
	if (NULL == fp) {
		fprintf(stderr, "Error opening dump file: %s\n", strerror(errno));
		return REPL_ERROR;
	}
	fsize = fwrite(result_buffer, 1, how_many_bytes, fp);
	fclose(fp);

	if (fsize != how_many_bytes) {
		fprintf(stderr, "Unknown error during write.\n");
		return REPL_ERROR;
	}

	return REPL_SUCCESS;
}

/*
 * Iterate through stdin, executing simple commands.
 *
 * Empty lines and lines starting with the hash sign # are ignored.
 * If any command returns nonzero, the program will abort.
 */
int main() {
	char* line;
	size_t len;
	REPL_ERROR_T repl_error;

	seed_file_state = alloca(sizeof(struct seed_file_state_struct));
	init_seed_file(seed_file_state);

	while (1) {

		line = NULL;
		repl_error = getline(&line, &len, stdin);
		
		if (-1 == repl_error) {
		        free(line);

			return EXIT_SUCCESS;
		}

		repl_error = repl_exec_line(line);
		free(line);
		if (REPL_SUCCESS != repl_error) {
			return repl_error;
		}
	}
}
