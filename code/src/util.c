//
// Created by jan on 5/24/15.
//

#define RANDOM_FILE_SIZE_IN_KB 500

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "util.h"
#include <errno.h>
#include <math.h>
#include <gmp.h>

int read_bits(uint32_t cnt, struct seed_file_state_struct *seed_file_state, uint8_t *result){

    // End-Position
    uint32_t end = seed_file_state->position + cnt;

    // Check, if enough data is available
    if(end > 8 * RANDOM_FILE_SIZE_IN_KB*1024){
        return 0;
    }

    // Iterate and write result
    uint32_t ctr;
    for(ctr = 0; ctr < cnt; ctr++){
        uint32_t byte_offset = seed_file_state->position / 8;
        uint8_t mask = 0x01 << (seed_file_state->position % 8);
        result[ctr] = (seed_file_state->file_content[byte_offset] & mask) > 0; // If mask == 0, this converts to 0/1
        seed_file_state->bits_read++;
	seed_file_state->position++;
    }
    return cnt;
}

uint8_t* read_bytes(uint32_t cnt, struct seed_file_state_struct *seed_file_state){
    // Look for an offset
    int bit_offset = seed_file_state->position % 8;
    // Skip the remaining-bytes of the word - everything else is just annoying.

    if(bit_offset > 0){
        seed_file_state->position += 8-bit_offset;
    }
    int b_offset = seed_file_state->position / 8;

    // Check, if there's enough data
    if(cnt + b_offset > RANDOM_FILE_SIZE_IN_KB * 1024){
        return NULL;
    }

    // Increment counter
    seed_file_state->bits_read += (cnt * 8);
    seed_file_state->position += (cnt * 8);

    // Return result pointer
    return &seed_file_state->file_content[b_offset];
}


/**
 * Initialize seed-file. Read data from filesystem
 * Returns-
 * FAILURE if an error occurs or the seed file is not the expected size
 * SUCCESS else
 */
ERROR_CODE_T init_seed_file(struct seed_file_state_struct *seed_file_state){
    const char *filename = "code/res/01_outputAES.rnd";
    //const char *filename = "/home/jan/ClionProjects/15ss-taoc/code/res/01_outputAES.rnd";

    const int filesize = 512*1000;

    FILE *f = fopen(filename,"rb");

    // Open file
    if (f == NULL){
        fprintf(stderr, "Error opening %s: %s\n", filename,strerror( errno ));
        return FAILURE;
    }

    // Read from file
    size_t bytes_read =  fread(seed_file_state->file_content, sizeof(uint8_t), filesize, f);
    seed_file_state->bits_read = 0;
    seed_file_state->position = 0;
    fclose(f);

    if(bytes_read == filesize)
    	return SUCCESS;
    else
    	return FAILURE;
}

ERROR_CODE_T get_random_number(mpz_t randint, uint32_t cnt, struct seed_file_state_struct* random_source) {
	const int order = 1; // most significant byte first
	const int endian = 0; // native endianness
	const int nails = 0; // skip no bits

	uint8_t* source = read_bytes(cnt, random_source);
	if (NULL == source)
		return FAILURE;
	// randint = cnt * sizeof(uint8_t) bytes from source
	mpz_import(randint, cnt, order, sizeof(uint8_t), endian, nails, source);
	return SUCCESS;
}

double get_byte_entropy(const uint8_t *data, size_t how_many_bytes) {
	int i;
	double entropy;
	int distribution[256];
	double relative_freq[256];

	memset(distribution, 0, sizeof(int) * 256);

	for (i = 0; i < how_many_bytes; i++)
		distribution[data[i]]++;
	for (i = 0; i < 256; i++)
		relative_freq[i] = distribution[i] / ((double) how_many_bytes);

	entropy = 0;
	for (i = 0; i < 256; i++) {
		if (0 != distribution[i])
			entropy -= relative_freq[i] * log2(relative_freq[i]);
	}

	return entropy;
}
/**
 * Extract a certain amount of bits - NO SANITY-CHECK!!
 * NOTE: Only full bytes are copied into the result buffer - will remain in the accumulator
 */
uint8_t* extract_bits(uint8_t *resultbuffer, mpz_t source, uint32_t count, struct acc_struct_gen_t *acc){
  //uint8_t* willy;
    uint8_t *ret = resultbuffer;
    // How many byte?
    uint32_t bytes = count >> 3;
    size_t countp = 0;

    if(bytes){ // Are there bytes to copy?
      //mpz_export(ret, &countp, -1, 1, -1, 0, source);
        memcpy(resultbuffer,source->_mp_d,bytes);

	ret = ret+bytes;
    }

    if (count % 8){ //Additional bits to copy?
        int i;
        for(i = bytes << 3; i < count;i++){
            acc->value <<= 1;
            acc->value |= mpz_tstbit(source, i);
            acc->offset++;
            if(acc->offset % 8 == 0){ // Got byte, going ahead
                acc->offset = 0;
                ret[0] = acc->value;
                ret = ret+1; //Move to next slide.
            }
        }
    }
  return ret; // Done
}
