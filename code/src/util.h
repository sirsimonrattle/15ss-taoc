//
// Created by jan on 5/24/15.
//

#ifndef PRNGS_UTIL_H
#define PRNGS_UTIL_H

#include <stdint.h>
#include <gmp.h>
#include <strings.h>

#define SEED_FILE_SIZE (512*1000)
// Up to 32 error codes
typedef enum ERROR_CODE {
	SUCCESS = 0,
	FAILURE = -1,
	ERR_UNKNOWN = 1,
	ERR_WRONG_ARGUMENT,
	ERR_FILE_NOT_EXISTS,
	ERR_FILE_READ,
	ERR_FILE_OPEN,
	ERR_FILE_SEEK
} ERROR_CODE_T;

#define ASSERT(x)       if((x) != SUCCESS) return (x)

// Checks whether the condition is satisfied, if not prints message and returns err_code
#define ASSERT_A(condition, err_code, message, ...) \
    if( !(condition) ){ \
    	fprintf(stderr, "ERROR: file %s: line %d: " message "\n", __FILE__, __LINE__, ##__VA_ARGS__); \
    	return (err_code); \
    }

/** Structure, representing the randome file state */
struct seed_file_state_struct{
    uint64_t bits_read;
    uint64_t position;
    uint8_t file_content [SEED_FILE_SIZE]; // Full content to be kept in system memory
};

/**
 * Reads N bits from a file.
 * Returns:
 * - Number of bits read
 * - Zero, if an error occured
 */
int read_bits(uint32_t cnt, struct seed_file_state_struct *seed_file_state, uint8_t *result);

/**
 * Reads N bytes from a file.
 * Returns:
 * - Pointer to seed file's data, if possible
 * - NULL, if there's not enough data
 */
uint8_t* read_bytes(uint32_t cnt, struct seed_file_state_struct *seed_file_state);


/**
 * Initialize seed-file. Read data from filesystem
 * Returns-
 * FAILURE if an error occurs or the seed file is not the expected size
 * SUCCESS else
 */
ERROR_CODE_T init_seed_file(struct seed_file_state_struct *seed_file_state);

/*
 * takes cnt many bytes from random_source to generate a mpz_t randint
 * Returns:
 * FAILURE if read_bytes was not successful,
 * SUCCESS otherwise
 */
ERROR_CODE_T get_random_number(mpz_t randint, uint32_t cnt, struct seed_file_state_struct* random_source);


/*
 * short and sweet:
 * calculate the byte entropy of data, a byte vector of size datalen
 */
double get_byte_entropy(const uint8_t *data, size_t datalen);

struct acc_struct_gen_t {
	uint8_t value;
	uint8_t offset;
};

uint8_t * extract_bits(uint8_t *resultbuffer, mpz_t source, uint32_t count, struct acc_struct_gen_t *acc);
#endif //seed_file_state

