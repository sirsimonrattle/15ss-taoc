#include <wmmintrin.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include "aesni.h"
/*
 * The code in this file is either taken directly, or adapted from:
 *   Intel (R) Advanced Encryption Standard (AES) New Instructions Set
 *   323641-001
 *   Revision 3.01
 *   September 2012
 *
 * Sources:
 *   cupid and Check_CPU_support_AES: taken from p. 31.
 *   AES_128_ASSIST, AES_128_KEY_Expansion: taken with minor modifications from p. 31.
 *   aesni_generate: adapted from p. 38.
 */

__m128i ctr, ONE;
/* 10 full rounds plus the last one */
__m128i key_schedule[11];

#define cpuid(func,ax,bx,cx,dx)\
	__asm__ __volatile__ ("cpuid":\
	"=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

int Check_CPU_support_AES() {
	unsigned int a, b, c, d;
	cpuid(1, a, b, c, d);
	return (c & 0x2000000);
}

inline __m128i AES_128_ASSIST(__m128i temp1, __m128i temp2) {
	__m128i temp3;
	temp2 = _mm_shuffle_epi32(temp2, 0xff);
	temp3 = _mm_slli_si128(temp1, 0x4);
	temp1 = _mm_xor_si128(temp1, temp3);
	temp3 = _mm_slli_si128(temp3, 0x4);
	temp1 = _mm_xor_si128(temp1, temp3);
	temp3 = _mm_slli_si128(temp3, 0x4);
	temp1 = _mm_xor_si128(temp1, temp3);
	temp1 = _mm_xor_si128(temp1, temp2);
	return temp1;
}

/*
 * This function cannot be compacted to a loop, because _mm_aeskeygenassist_si128
 * takes an immediate value for the second argument.
 * Minor change: Arguments to this function used to be of type unsigned char *
 */
void AES_128_Key_Expansion(__m128i *userkey, __m128i *key) {
	__m128i temp1, temp2;
	__m128i *Key_Schedule = key;
	temp1 = _mm_loadu_si128(userkey);
	Key_Schedule[0] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x1);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[1] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x2);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[2] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x4);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[3] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x8);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[4] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x10);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[5] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x20);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[6] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x40);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[7] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x80);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[8] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x1b);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[9] = temp1;
	temp2 = _mm_aeskeygenassist_si128(temp1, 0x36);
	temp1 = AES_128_ASSIST(temp1, temp2);
	Key_Schedule[10] = temp1;
}

GEN_ERROR_T aesni_init(struct seed_file_state_struct *seed_file,
		uint32_t num_bytes) {
	__m128i iv;
	uint8_t *seed;

	if (!Check_CPU_support_AES()) {
		fprintf(stderr,
				"This CPU doesn't seem to support the AES-NI instructions.\n");
		return GEN_FAILURE;
	}

	if (0 != num_bytes % sizeof(__m128i )) {
		fprintf(stderr, "AES-NI only supports output in %ld-byte increments.\n",
				sizeof(__m128i ));
		return GEN_FAILURE;
	}

	// Intel's world is little-endian
	ctr = _mm_set_epi32(0, 0, 0, 0);
	ONE = _mm_set_epi32(0, 0, 0, 1);
	seed = read_bytes(16, seed_file);
	iv = _mm_loadu_si128((void*) seed);

	// key expansion
	AES_128_Key_Expansion(&iv, key_schedule);

	return GEN_SUCCESS;
}

GEN_ERROR_T aesni_generate(uint8_t *outbuffer,
		struct seed_file_state_struct *seed_file, uint32_t num_bytes) {
	int gen_round, aes_round, num_gen_rounds;
	__m128i acc;
	num_gen_rounds = num_bytes / sizeof(__m128i );
	__m128i *outblocks = (__m128i *) outbuffer;

	for (gen_round = 0; gen_round < num_gen_rounds; gen_round++) {
		// increase counter
		ctr = _mm_add_epi64(ctr, ONE);
		acc = ctr;
		for (aes_round = 0; aes_round < 10; aes_round++) {
			acc = _mm_aesenc_si128(acc, key_schedule[aes_round]);
		}
		acc = _mm_aesenclast_si128(acc, key_schedule[aes_round]);
		_mm_storeu_si128(&(outblocks[gen_round]), acc);
	}

	return GEN_SUCCESS;
}

GEN_ERROR_T aesni_free(void) {
	return GEN_SUCCESS;
}

REPL_ERROR_T aesni_set_parameter(const char *key,
		const char *value) {
	return REPL_INVALID_KEY;
}

REPL_ERROR_T aesni_load_default_parameters() {
	return REPL_SUCCESS;
}

REPL_ERROR_T aesni_show_parameters(const char *experiment_name,
		FILE *msg_stream) {
	return REPL_SUCCESS;
}
