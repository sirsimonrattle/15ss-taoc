#ifndef LCG_H
#define LCG_H

#include "generators.h"

DECLARE_GEN_REPL_FUNCTIONS(lcg)
void lcg_extract_bits();
#include <string.h>
#endif // _LCG_H
