#ifndef GENERATORS_H
#define GENERATORS_H

#include "util.h"
#include <strings.h>
#include <stdio.h>
#define BYTES_TO_GENERATE (512 * 1000)

/* Error codes */
typedef enum GEN_ERROR {
	GEN_SUCCESS = 0,
	GEN_FAILURE = -1

} GEN_ERROR_T;
typedef enum REPL_ERROR {
	REPL_SUCCESS = 0,
	REPL_ERROR = -1,
	REPL_INVALID_KEY = 1,
	REPL_INVALID_VALUE,
	REPL_GEN_ERROR_INIT,
	REPL_GEN_ERROR_RUN,
	REPL_GEN_ERROR_FREE
} REPL_ERROR_T;

#define ACCEPT_PARAMETER(param, fn_conv, fn_check) \
	do { \
	if(!strcasecmp(#param, key)) { \
		ival = fn_conv(value); \
		if(SUCCESS != (fn_check(ival))) { \
			return REPL_INVALID_VALUE; \
		} else { \
			params.param = ival; \
			return REPL_SUCCESS;\
		} \
	} \
	} while (0)

#define PARAMETER_IS_POSITIVE_INTEGER(i) ((i) >= 0 ? SUCCESS : FAILURE)
#define ACCEPT_PARAMETER_pi(param) ACCEPT_PARAMETER(param, atoi, PARAMETER_IS_POSITIVE_INTEGER)
#define SHOW_PARAMETER_pi(param) \
	fprintf(msg_stream, "%s," #param ",%d\n", experiment_name, params.param)


/*
 * To work with the scriptable execution framework, the following functions must be implemented:
 *
 * GEN_ERROR_T init(struct seed_file_state_struct *seed_file, uint32_t num_bytes)
 * GEN_ERROR_T generate(uint8_t *outbuffer, struct seed_file_state_struct *seed_file, uint32_t num_bytes)
 * GEN_ERROR_T free(void)
 * REPL_ERROR_T set_parameter(const char *key, const char *value)
 * REPL_ERROR_T load_default_parameters()
 * REPL_ERROR_T show_parameters(const char *experiment_name, FILE *msg_stream)
 */
#define DECLARE_GEN_REPL_FUNCTIONS(generator_prefix) \
	GEN_ERROR_T  generator_prefix ## _init(struct seed_file_state_struct *seed_file, uint32_t num_bytes); \
	GEN_ERROR_T  generator_prefix ## _generate(uint8_t *outbuffer, struct seed_file_state_struct *seed_file, uint32_t num_bytes); \
	GEN_ERROR_T  generator_prefix ## _free(void); \
	REPL_ERROR_T generator_prefix ## _set_parameter(const char *key, const char *value); \
	REPL_ERROR_T generator_prefix ## _load_default_parameters(void); \
	REPL_ERROR_T generator_prefix ## _show_parameters(const char *experiment_name, FILE *msg_stream);




#endif
