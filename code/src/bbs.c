#include "bbs.h"
/*
 * Parameters for the Blum-Blum-Shub generator
 *
 * n:
 *   size (in bytes) of the modulus and state
 * bits_per_round:
 *   how many bits to extract from the state in each round
 */
struct {
	int n;
	int bits_per_round;
} params;
mpz_t N, s;
uint32_t bits_per_round;

GEN_ERROR_T bbs_init(struct seed_file_state_struct *random_source,
		uint32_t output_size) {
	mpz_t p, q;

	mpz_inits(N, p, q, s, NULL);

	bits_per_round = params.bits_per_round;

	if (SUCCESS != get_random_number(p, params.n / 2, random_source))
		return GEN_FAILURE;
	if (SUCCESS != get_random_number(q, params.n / 2, random_source))
		return GEN_FAILURE;
	if (SUCCESS != get_random_number(s, params.n, random_source))
		return GEN_FAILURE;

	find_blum_factor(p);
	find_blum_factor(q);

	if (!mpz_cmp(p, q)) { //if p and q are equal, take the next blum integer for q
		mpz_add_ui(q, q, 1);
		find_blum_factor(q);
	}

	mpz_mul(N, p, q); //calculate N = p*q

	mpz_clears(p, q, NULL);
	return GEN_SUCCESS;
}

void find_blum_factor(mpz_t p) {
	mpz_t tmp;
	mpz_init(tmp);

	mpz_mod_ui(tmp, p, 4); // tmp = p % 4
	while (mpz_cmp_ui(tmp, 3) != 0) { // tmp != 3
		mpz_add_ui(p, p, 1); // p++
		mpz_mod_ui(tmp, p, 4);  // tmp = p % 4
	}
	// we have now: p == 3 (mod 4)

	while (!mpz_probab_prime_p(p, 25))
		mpz_add_ui(p, p, 4);

	mpz_clear(tmp);
}

/* 
 * runs the Blum-Blum-Shub generator by calling bbs_gen with fixed parameter values
 * random values are taken from random_source
 * results are written to resultbuffer
 * returns 0 on success, nonzero on any failure
 */
GEN_ERROR_T bbs_generate(uint8_t * resultbuffer,
		struct seed_file_state_struct *random_source, uint32_t output_size) {

	int i, j, num_rounds, num_extra_bits;
	int num_bits = output_size * 8;
	int output_bit = 0;

	num_extra_bits = num_bits % bits_per_round; // in the last iteration only r <= k bits are needed to reach the exact size of result buffer (avoids segmentation faults)
	num_rounds = num_bits / bits_per_round; // l+1 iterations needed in order to produce output_size bytes of output, if k bits are taken from each iteration (and r bits taken from the last iteration)

	/*
	 * random values are taken from random_source
	 * n: size of the modulus N in bytes
	 * output_size: size of the output in bytes
	 * k: number of least significant bits used for the output in each iteration
	 * results are written to result buffer
	 * returns number of bytes written to resultbuffer, returns 0 in case the parameter initialisation of p, q and seed x fails
	 */

	/* executes BBS generation */

	mpz_mod(s, s, N);		// s = s % N
	for (i = 0; i <= num_rounds; i++) {
		mpz_mul(s, s, s);	// s = s^2
		mpz_mod(s, s, N);	// s = s % N
		if (i == num_rounds)
			bits_per_round = num_extra_bits;
		for (j = 0; j < bits_per_round; j++) {
			resultbuffer[output_bit / 8] |= mpz_tstbit(s, j)
					<< (output_bit % 8);
			output_bit++;
		}
	}

	return GEN_SUCCESS;
}

GEN_ERROR_T bbs_free(void) {
	mpz_clears(s, N, NULL);	//free memory
	return GEN_SUCCESS;
}

REPL_ERROR_T bbs_load_default_parameters() {
	params.n = 375;
	params.bits_per_round = 1;

	return REPL_SUCCESS;
}

REPL_ERROR_T bbs_set_parameter(const char *key, const char *value) {
	int ival;

	ACCEPT_PARAMETER_pi(n);
	ACCEPT_PARAMETER_pi(bits_per_round);

	return REPL_SUCCESS;
}

REPL_ERROR_T bbs_show_parameters(const char *experiment_name, FILE *msg_stream) {
	SHOW_PARAMETER_pi(n);
	SHOW_PARAMETER_pi(bits_per_round);

	return REPL_SUCCESS;
}
