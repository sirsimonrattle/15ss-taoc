#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "littlewood.h"

/*
 * We produce at most 512000 * 8 bits. 
 * This number is 22 bits long.
 * Littlewood took the last two numbers from a log-table that 
 * mapped 5-digit inputs to 7-digit outputs.
 * 
 * In order to have a precise value at the d-th post-decimal value,
 * we must calculate with at least 22 + d bits precision.
 */
int precision;
#define ROUNDING_MODE MPFR_RNDN
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/*
 * Parameters for Littlewood's generator:
 *
 * seed_size_pow2:
 *   How many bits should be used to determine the initial value x.
 * d
 *   Use the d-th post-decimal bit during extraction
 */
struct {
	int seed_len;
	int d;
} params;
int n, d, precision;
mpz_t x, i;
mpfr_t acc, TWO;

void littlewood_print_info() {
	char * str;

	str = mpz_get_str(NULL, 2, x);
	printf("x = %s\n", str);
	free(str);

	printf("n = %d\n", n);
	printf("d = %d\n", d);

	str = mpz_get_str(NULL, 10, i);
	printf("This generator has produced %s bits.\n", str);
	free(str);
}

int littlewood_single_bit() {
	/*
	 * acc = log_2((x + i) >> n)
	 * acc = fractional-part(acc << d-1)
	 * // the d-th post-decimal bit of log2((x+i) << n) is now the first post-decimal bit
	 * i++
	 * return abs(acc) > 0.5
	 */
	mpfr_set_z(acc, x, ROUNDING_MODE);
	mpfr_add_z(acc, acc, i, ROUNDING_MODE);
	mpfr_mul_2si(acc, acc, (-1) * n, ROUNDING_MODE);
	mpfr_log2(acc, acc, ROUNDING_MODE);

	mpfr_mul_2si(acc, acc, d - 1, ROUNDING_MODE);
	mpfr_frac(acc, acc, ROUNDING_MODE);
	mpfr_abs(acc, acc, ROUNDING_MODE);

	mpz_add_ui(i, i, 1);
	return mpfr_cmp_d(acc, 0.5) > 0;
}

GEN_ERROR_T littlewood_init(struct seed_file_state_struct *seed_file,
		uint32_t num_bytes) {
	int x_len = params.seed_len;
	uint8_t x_bits_raw[x_len];
	int ret, i;
	char x_bits[x_len + 1];

	ret = read_bits(x_len, seed_file, x_bits_raw);
	if (x_len != ret)
		return -1;

	for (i = 0; i < x_len; i++)
		x_bits[i] = x_bits_raw[i] ? '1' : '0';
	x_bits[x_len] = '\0';

	return littlewood_str_init(x_bits, num_bytes);
}

GEN_ERROR_T littlewood_str_init(char * x_bits, uint32_t num_bytes) {
	int pre_decimal_digits, num_bytes_len;

	if (-1 == mpz_init_set_str(x, x_bits, 2))
		return GEN_FAILURE;

	mpz_init_set_ui(i, 0);

	n = params.seed_len;
	d = params.d;

	num_bytes_len = ceil(log2(8 * num_bytes));
	pre_decimal_digits = MAX(num_bytes_len, params.seed_len);
	precision = pre_decimal_digits + d;

	mpfr_init2(TWO, precision);
	mpfr_set_ui(TWO, 2, ROUNDING_MODE);
	mpfr_init2(acc, precision);

	return GEN_SUCCESS;
}

GEN_ERROR_T littlewood_generate(uint8_t *resultbuffer,
		struct seed_file_state_struct *seed_file_state,
		uint32_t bytes_to_generate) {
	int next_bit, output_bit;

	for (output_bit = 0; output_bit < 8 * bytes_to_generate; output_bit++) {
		next_bit = littlewood_single_bit();
		resultbuffer[output_bit / 8] |= next_bit << (output_bit % 8);
	}

	return GEN_SUCCESS;
}

GEN_ERROR_T littlewood_free() {
	mpz_clear(x);
	mpz_clear(i);
	mpfr_clear(acc);
	mpfr_clear(TWO);
	mpfr_free_cache();

	return GEN_SUCCESS;
}

/*
 Configures the generator with the specified values from the exercise task.
 n = 5
 d = 7
 x will be taken as n bits from the source
 */
REPL_ERROR_T littlewood_load_default_parameters() {
	params.seed_len = 5;
	params.d = 22 + 7;

	return REPL_SUCCESS;
}

REPL_ERROR_T littlewood_set_parameter(const char *key, const char *value) {
	int ival;

	ACCEPT_PARAMETER_pi(seed_len);
	ACCEPT_PARAMETER_pi(d);

	return REPL_SUCCESS;
}

REPL_ERROR_T littlewood_show_parameters(const char *experiment_name,
		FILE *msg_stream) {
	SHOW_PARAMETER_pi(seed_len);
	SHOW_PARAMETER_pi(d);
	return REPL_SUCCESS;
}

