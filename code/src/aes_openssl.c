//
// Created by J.Luehr on 11/17/15.
//

#include "aes_openssl.h"

// Parameter: keysize - AES (as specified) support 128, 192 and 256 bits
struct {
    uint16_t keysize;
} params;

EVP_CIPHER_CTX *ctx;
uint8_t *key; // 16-Byte key
uint8_t *iv; // 16-Byte key
const uint8_t aes_blocksize = 128 / 8;


// Following: https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption

GEN_ERROR_T aes_openssl_init(struct seed_file_state_struct *seed_file, uint32_t num_bytes) {
    if(!(ctx = EVP_CIPHER_CTX_new())){ // OpenSSL Boiler-Plate-Code
        return GEN_FAILURE;
    }
    // Seed key based on random data
    key = read_bytes(params.keysize / 8,seed_file);

    // Seed iv, too.
    iv = read_bytes(aes_blocksize,seed_file);

    // OpenSSL boilerplate code
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    OPENSSL_config(NULL);


    // Get cipher based on key-size
    int init_result = 0;
    if(params.keysize == 128){
        init_result = EVP_EncryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key, iv);
    } else if (params.keysize == 192){
        init_result = EVP_EncryptInit_ex(ctx, EVP_aes_192_ctr(), NULL, key, iv);
    } else if(params.keysize == 256) {
        init_result = EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), NULL, key, iv);
    }
    if(init_result != 1){ // OpenSSL init error or unsupported keysize
        return GEN_FAILURE;
    }
    return GEN_SUCCESS;
}
GEN_ERROR_T aes_openssl_generate(uint8_t *outbuffer, struct seed_file_state_struct *seed_file, uint32_t num_bytes) {
    uint8_t plaintext[num_bytes]; // We don't care about init ... it's random anyway
    int len; // Ciphertext-Length ... we don't care

    if(num_bytes % aes_blocksize){
        printf("Unable to generate %u bytes - bytes need to be a multiple of the blocksize (128 bit = 16 byte)\n",num_bytes);
        return GEN_FAILURE;
    }
    if(EVP_EncryptUpdate(ctx, outbuffer, &len, plaintext, num_bytes) != 1){
        return GEN_FAILURE;
    }
    return GEN_SUCCESS;
}

GEN_ERROR_T aes_openssl_free(void) {
    EVP_cleanup();
    ERR_free_strings();

    return GEN_SUCCESS;
}

REPL_ERROR_T  aes_openssl_set_parameter(const char *key, const char *value) {
    int ival;
    ACCEPT_PARAMETER_pi(keysize);
    return REPL_INVALID_KEY;
}

REPL_ERROR_T aes_openssl_load_default_parameters() {
    params.keysize = 128;
    return REPL_SUCCESS;
}

REPL_ERROR_T aes_openssl_show_parameters(const char *experiment_name, FILE *msg_stream) {
    SHOW_PARAMETER_pi(keysize);
    return REPL_SUCCESS;
}