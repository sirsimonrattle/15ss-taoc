#ifndef BBS_GEN_H
#define BBS_GEN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>
#include <math.h>
#include "util.h"
#include "generators.h"


DECLARE_GEN_REPL_FUNCTIONS(bbs)

/*
 * Find the next blum factor that is greater than or equal to the provided p
 * and write it back to p
 *
 * A blum factor is a prime p with p == 3 (mod 4).
 * A blum integer is the product of two blum factors.
 */
void find_blum_factor(mpz_t p);

#endif
