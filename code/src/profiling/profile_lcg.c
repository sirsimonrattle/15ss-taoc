// Profiling LCG
// Created by jan on 12/1/15.
//


#include "../lcg.h"
int main(){
    // Default Parameters
    // 1 MB of randomness
    const int bytes_to_generate = 5 * 1024 * 1024;

    uint8_t outbuffer[bytes_to_generate];
    printf("starting \n");

    // Init everything
    struct seed_file_state_struct seed_file_state;
    init_seed_file(&seed_file_state);
    printf("Init seed file complete \n");
    lcg_load_default_parameters();

    printf("Parameter load complete \n");
    lcg_init(&seed_file_state,bytes_to_generate);
    printf("Init complete \n");


    // Start generation
    if(lcg_generate(outbuffer,&seed_file_state,1024*1024) != GEN_SUCCESS){
        printf("Error executing test_aes_Openssl\n");
    } else {
        printf("Success \n");
    }

    // Clean-Up
    lcg_free();
    return 0;
}