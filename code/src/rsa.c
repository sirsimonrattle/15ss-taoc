#include "rsa.h"

/*
 * Parameters for the RSA generator
 *
 * n:
 *   size (in bytes) of the modulus and state
 * bits_per_round:
 *   how many bits to extract from the state in each round
 * e:
 *   exponent used in the calculation: x_i = x_{i-1}^e (mod N).
 *   If e and phi(N) are not coprime, the next coprime number greater than e is used.
 */
struct {
	int n;
	int bits_per_round;
	int e;
} params;
mpz_t N, x, e;
uint32_t bits_per_round;

GEN_ERROR_T rsa_init(struct seed_file_state_struct *random_source,
		uint32_t how_many_bytes) {

	mpz_t p, q, phi, tmp1, tmp2;

	bits_per_round = params.bits_per_round;

	mpz_inits(N, p, q, x, phi, tmp1, tmp2, e, NULL);

	// get random values for p, q, x
	if (SUCCESS != get_random_number(p, params.n / 2, random_source))
		return GEN_FAILURE;
	if (SUCCESS != get_random_number(q, params.n / 2, random_source))
		return GEN_FAILURE;
	if (SUCCESS != get_random_number(x, params.n, random_source))
		return GEN_FAILURE;

	// p and q must be prime
	mpz_nextprime(p, p);
	mpz_nextprime(q, q);

	//if p and q are equal, take the next prime for q
	if (0 == mpz_cmp(p, q)) {
		mpz_nextprime(q, q);
	}

	mpz_mul(N, p, q); //calculate N = p*q

	// phi = (p-1)*(q-1)
	mpz_sub_ui(tmp1, p, 1);
	mpz_sub_ui(tmp2, q, 1);
	mpz_mul(phi, tmp1, tmp2);

	/* Find a suitable exponent e
	 * e = params->e
	 * while (gcd(e, phi) != 1){
	 *   if (e == phi - 1)
	 *     return FAILURE;
	 *   e++;
	 * }
	 */
	mpz_set_ui(e, params.e);
	mpz_sub_ui(tmp1, phi, 1); // tmp1 = phi - 1
	mpz_gcd(tmp2, e, phi);  // tmp2 = gcd(e, phi)
	while (0 != mpz_cmp_ui(tmp2, 1)) { // tmp2 != 1
		mpz_add_ui(e, e, 1); // e++
		if (0 == mpz_cmp(e, tmp1)) { // e == tmp1
			fprintf(stderr, "Error: there is no e > %d with gcd(e, phi) == 1\n",
					params.e);
			return GEN_FAILURE;
		}
		mpz_gcd(tmp2, e, phi); // tmp2 = gcd(e, phi)
	}

	mpz_clears(p, q, phi, tmp1, tmp2, NULL);
	return GEN_SUCCESS;
}

/*
 * runs the RSA - generator by calling rsa_gen with fixed parameter values
 * random values are taken from random_source
 * results are written to resultbuffer
 * returns 0 on success, nonzero on any failure
 */
GEN_ERROR_T rsa_generate(uint8_t *resultbuffer,
		struct seed_file_state_struct *random_source, uint32_t num_bytes) {
	int i, num_rounds;
	int num_bits = 8 * num_bytes;

	num_rounds = num_bits / bits_per_round;

	uint8_t *result_buffer_offset = resultbuffer;
	struct acc_struct_gen_t accumulator;
	accumulator.offset = 0;

	mpz_mod(x, x, N); // x = x mod N
	for (i = 0; i <= num_rounds; i++) {
		mpz_powm(x, x, e, N); // x = x^e mod N
		result_buffer_offset = extract_bits(result_buffer_offset,x,bits_per_round,&accumulator); //Extract, move forward

	}

	// Test: Bits not copied bytewise, yet?
	if(accumulator.offset){
		result_buffer_offset[0] = accumulator.value;
	}

	return GEN_SUCCESS;
}

GEN_ERROR_T rsa_free(void) {
	mpz_clears(x, N, e, NULL);
	return GEN_SUCCESS;
}

REPL_ERROR_T rsa_load_default_parameters() {
	params.n = 512;
	params.bits_per_round = 1;
	params.e = 3;

	return REPL_SUCCESS;
}

REPL_ERROR_T rsa_set_parameter(const char *key, const char *value) {
	int ival;

	ACCEPT_PARAMETER_pi(n);
	ACCEPT_PARAMETER_pi(bits_per_round);
	ACCEPT_PARAMETER_pi(e);

	return REPL_INVALID_KEY;
}
REPL_ERROR_T rsa_show_parameters(const char *experiment_name, FILE *msg_stream) {
	SHOW_PARAMETER_pi(n);
	SHOW_PARAMETER_pi(bits_per_round);
	SHOW_PARAMETER_pi(e);

	return REPL_SUCCESS;
}
