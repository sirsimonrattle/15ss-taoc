// It works and generates random bytes, the entropy does not look strange anymore

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "util.h"
#include <gmp.h>
#include <assert.h>
#include "nisan_wigderson.h"
#include <math.h>

#define EXP ((s+1) / 2)

/*
 * This generator uses (k, n, s, t)-designs where:
 *   k = s^2
 *   n = s^(t+1)
 * Iff s > t and s is a prime power, such a design exists and can be constructed efficiently.
 */
struct {
	int s;
	int t;
} params;
uint8_t *D, *x;
int k, n, s, t;

/*
 * For coefficients a = (a_0, ..., a_t) in (F_q)^(t+1),
 * next_poly(a, t, q) finds values for a such that
 * after q^(t+1) calls, a will have iterated (F_q)^(t+1).
 *
 * It does that by treating a as a number in F_(q^(t+1))
 * and calculating a + 1
 */
void next_poly(int *coeffs, int degree, int modulus) {
	int i;

	for (i = 0; i <= degree; i++) {
		coeffs[i] = (coeffs[i] + 1) % modulus;
		if (coeffs[i] != 0)
			break;
	}
}

/*
 * For coefficients a = (a_0, ..., a_t) in (F_q)^(t+1),
 * evaluate_poly(a, t, q, x) will evaluate f(x) in F_q where
 * f(x) = a_0 * x^0 + ... + a_t * x^t
 */
int evaluate_poly(int *coeffs, int degree, int modulus, int x) {
	int i;
	int ret = 0;

	// Horner schema
	for (i = degree; i >= 0; i--) {
		ret = ret * x + coeffs[i];
	}

	return ret % modulus;
}

/*
 * For given:
 *   s, a prime power
 *   t, an integer < s
 *   D, a buffer of size at least s^(t+2)
 * generate_design(D, s, t) will construct a (s^2, s^(t+1), s, t)-design.
 * For any i and j, the value S_i[j] is in D[i * s + j].
 */
void construct_design(uint8_t *D, int s, int t) {
	int i, x, y;
	int n = pow(s, t + 1);
	int *coeffs = alloca(sizeof(int) * (t + 1));
	memset(coeffs, 0, sizeof(int) * (t + 1));

	for (i = 0; i < n; i++) {
		for (x = 0; x < s; x++) {
			y = evaluate_poly(coeffs, t, s, x);
			D[i * s + x] = (y * s + x) % (s * s);
		}
		next_poly(coeffs, t, s);
	}
}

GEN_ERROR_T nw_init(struct seed_file_state_struct *seed_file,
		uint32_t output_size) {
	k = pow(params.s, 2);
	n = pow(params.s, params.t + 1);
	s = params.s;
	t = params.t;

	// Instead of checking for prime powers, we just check for odd numbers
	if (!((s % 2 == 1) && (s > t))) {
		fprintf(stderr,
				"Error: Nisan-Wigderson requires s a prime power and s > t\n");
		return GEN_FAILURE;
	}

	D = malloc(sizeof(uint8_t) * pow(s, t + 2));
	construct_design(D, s, t);

	x = malloc(sizeof(uint8_t) * k);

	return GEN_SUCCESS;
}

GEN_ERROR_T nw_free() {
	free(D);
	free(x);

	return GEN_SUCCESS;
}

/*
 * This function picks the values from x with indices in design #d
 * and computes parity(a+b), where:
 *   D_d = D + d * s, the design #d,
 *   in = (in_0, ..., in_(s - 1)) = (x[D_d[0]], ..., x[D_d[s - 1]])
 *   a = (in_0, ..., in_(s // 2)) in B^(s // 2 + 1)
 *   b = (in_(s // 2), ..., in_(s - 1)) in B^(s // 2 + 1)
 */
static uint8_t f(uint8_t *x, int d) {
	// f_in is n bytes long
	// f_tmp is ceil(n/2) bytes long
	int i;
	uint8_t *a_p, *b_p, *D_d;
	uint8_t a_i, b_i, carry, parity;

	/*
	 * The current design is at D_d = D + d*s.
	 *
	 * Vector a is at x[D_d[0]] up to x[D_d[s // 2]], including.
	 * Vector b is at x[D_d[s // 2]] up to x[D_d[s - 1]], including.
	 * Note that for odd s, (s // 2) + (s // 2) == s - 1.
	 *
	 * Let a_p = D_p and b_p = D_p + s // 2.
	 * Then, we have easier to read:
	 * Vector a is at x[a_p[0]] up to x[b_p[s // 2]], including.
	 * Vector b is at x[b_p[0]] up to x[b_p[s // 2]], including.
	 *
	 * Therefore, we have for 0 <= i <= s // 2:
	 *   a_i = x[a_p[i]]
	 *   b_i = x[b_p[i]]
	 *
	 * To find sum = a + b with carry, a full adder is swept over a and b:
	 *   sum_i = a_i xor b_i xor carry_i,
	 *   carry_(i+1) = (a_i and b_i) or (carry_i and (a_i xor  b_i))
	 *
	 * To find parity(sum), we fold the vector sum with xor.
	 *
	 * The value of sum is never needed as a whole, so the parity is calculated directly from the components of it.
	 */

	D_d = D + d * s;
	a_p = D_d;
	b_p = D_d + (s / 2);
	carry = 0;
	parity = 0; // neutral element for XOR
	for (i = 0; i <= s / 2; i++) {
		a_i = x[a_p[i]];
		b_i = x[b_p[i]];
		parity ^= a_i ^ b_i ^ carry;
		carry = (a_i & b_i) | (carry & (a_i ^ b_i));
	}

	return parity;
}

GEN_ERROR_T nw_generate(uint8_t *resultbuffer,
		struct seed_file_state_struct *seed_file_state, uint32_t num_bytes) {
	int num_bits = 8 * num_bytes;
	// Declaration section
	int design = 0, output_bit;
	uint8_t next_bit;

	// Generate pseudorandom bits into resultBits array
	for (output_bit = 0; output_bit < num_bits; output_bit++) {
		if (0 == (output_bit % n))
			read_bits(k, seed_file_state, x);

		next_bit = f(x, design);
		resultbuffer[output_bit / 8] |= next_bit << (output_bit % 8);
		design = (design + 1) % n;
	}

	return GEN_SUCCESS;
}

REPL_ERROR_T nw_set_parameter(const char *key, const char *value) {
	int ival;

	ACCEPT_PARAMETER_pi(s);
	ACCEPT_PARAMETER_pi(t);

	return REPL_INVALID_KEY;
}

REPL_ERROR_T nw_load_default_parameters() {
	params.s = 131;
	params.t = 2;

	return REPL_SUCCESS;
}

REPL_ERROR_T nw_show_parameters(const char *experiment_name, FILE *msg_stream) {
	SHOW_PARAMETER_pi(s);
	SHOW_PARAMETER_pi(t);

	return REPL_SUCCESS;
}
