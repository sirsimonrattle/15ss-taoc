#include "aes.h"
#include <stdint.h>
#include <stdio.h>

/* Use the ECB functions from tiny-AES128-C */
#define CBC 0
#define ECB 1
#include "tinyaes/aes.h"
#include "tinyaes/aes.c"
#undef CBC
#undef ECB

uint8_t iv[16], ctr[16], ov[16];

GEN_ERROR_T aes_init(struct seed_file_state_struct *seed_file,
		uint32_t num_bytes) {
	uint8_t *seed = read_bytes(16, seed_file);

	memcpy(iv, seed, 16 * sizeof(uint8_t));
	memset(ctr, 0, 16 * sizeof(uint8_t));

	return GEN_SUCCESS;
}

GEN_ERROR_T aes_generate(uint8_t *outbuffer,
		struct seed_file_state_struct *seed_file, uint32_t num_bytes) {
	int output_byte, i;

	for(output_byte = 0; output_byte < num_bytes; output_byte++){
		if(0 == output_byte % 16){
			// increase counter
			for(i = 0; i < 16; i++){
				ctr[i]++;
				if(0 != ctr[i])
					break;
			}
			// run AES to generate 16 bytes
			AES128_ECB_encrypt(ctr, iv, ov);
		}

		outbuffer[output_byte] = ov[output_byte % 16];
	}

	return GEN_SUCCESS;
}

GEN_ERROR_T aes_free(void) {
	return GEN_SUCCESS;
}

REPL_ERROR_T aes_set_parameter(const char *key,
		const char *value) {
	return REPL_INVALID_KEY;
}

REPL_ERROR_T aes_load_default_parameters() {
	return REPL_SUCCESS;
}

REPL_ERROR_T aes_show_parameters(const char *experiment_name,
		FILE *msg_stream) {
	return REPL_SUCCESS;
}

