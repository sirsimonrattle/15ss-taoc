/**
 * Linear congruential generator by Johannes
 * 
 * Based on true randomness from the file '01_outputAES.rnd', the
 * generator produces 500 kB of pseudo-randomness.
 *
 * Last modified: 21.06.2015
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include "lcg.h"

#define SELECT_RANDOMLY -1

/*
 * Parameters for the LCG generator
 *
 * modulus n is next_prime(2**modulus_size_pow2)
 * s and t are random values of the specified byte length
 * initial state is seed_len random bytes
 * k is how many bits are extracted in each round
 *
 */
struct {
	int modulus_size_pow2;
        int prime_modulus;
        int s;
        int t;
	int seed_len;
	int k;
} params;
mpz_t state;
mpz_t tmp_state; // Temporary state pointer

mpz_t modulus;
mpz_t lcg_s;
mpz_t lcg_t;
int bits_per_round; // How many bits to extract on each roun
int num_rounds;
int i, j;
uint8_t *buffer;
/**
 * De-facto main function to be called from other module to run the LCG.
 * resultbuffer should better be 512000 byte in size. seed_file_state should
 * contain the true random data to be used for generation.
 */
GEN_ERROR_T lcg_generate(uint8_t *result_buffer, struct seed_file_state_struct *seed_file_state, uint32_t how_many_bytes) {

	buffer = result_buffer;
	// Since we extract bits, an additional accumulator is needed
	struct acc_struct_gen_t accumulator;
	accumulator.offset = 0;

	uint8_t *result_buffer_offset = result_buffer;
	uint16_t bytes_per_round = bits_per_round / 8;

	//gmp_printf("s = %Zd, t = %Zd\n", lcg_s, lcg_t);

	for (i = 0; i < num_rounds; i++) {
		mpz_mul(state, state, lcg_s);
		mpz_add(state, state, lcg_t);
		mpz_mod(state, state, modulus);
		result_buffer_offset = extract_bits(result_buffer_offset,state,bits_per_round,&accumulator); //Extract, move forward
	}

	// Test: Bits not copied bytewise, yet?
	if(accumulator.offset){
		result_buffer_offset[0] = accumulator.value;
	}
	/*
	printf("ret = [");
	for(i = 0; i < how_many_bytes; i++) {
	  printf("%x ", result_buffer[i]);
	}
	printf("]\n");
	*/
	return GEN_SUCCESS;
}



GEN_ERROR_T lcg_init(struct seed_file_state_struct *seed_file,
		uint32_t how_many_bytes) {
	mpz_inits(state, lcg_s, lcg_t, modulus, NULL);
	bits_per_round = params.k;

	
	// Seed is always random
	if (SUCCESS != get_random_number(state, params.seed_len, seed_file))
	      return GEN_FAILURE;

	// For the modulus: If params.prime_modulus is set, generate it randomly
	if (params.prime_modulus) {
	  if (SUCCESS != get_random_number(modulus, params.modulus_size_pow2>>3, seed_file))
		return GEN_FAILURE;	  
	} else {
	  // Otherwise modulus is 2^params.modulus_size_pow2
	  mpz_ui_pow_ui(modulus, 2, params.modulus_size_pow2);
	}
	
	// For paramters s and t look whether it should be generated from the random file
	if(params.s == SELECT_RANDOMLY) {
	  if (SUCCESS != get_random_number(lcg_s, params.modulus_size_pow2>>3, seed_file))
		return GEN_FAILURE;
	} else {
	  mpz_set_ui(lcg_s, params.s);
	}
	
	if(params.t == SELECT_RANDOMLY) {
	  if (SUCCESS != get_random_number(lcg_t, params.modulus_size_pow2>>3, seed_file))
	    return GEN_FAILURE;
	} else {
	  mpz_set_ui(lcg_t, params.t);
	}


	int num_bits = how_many_bytes * 8;

	//if(num_bits % bits_per_round){
	//	printf("ERROR: Total bits is not a multiple of bits per round\n");
	//	return GEN_FAILURE;
	//}

	num_rounds = num_bits / bits_per_round;	// l+1 iterations needed in order to produce output_size bytes of output, if k bits are taken from each iteration (and r bits taken from the last iteration)

	// Temporary state - Hand-made init.
	// First state: seed
	tmp_state->_mp_size = modulus->_mp_size; //Size: Size of modulus!
	tmp_state->_mp_d = state->_mp_d;
	tmp_state->_mp_alloc =  modulus->_mp_size; // No worries, we're in the seed-file anyway

	return GEN_SUCCESS;
}

REPL_ERROR_T lcg_load_default_parameters() {
	params.modulus_size_pow2 = 2048;
	params.s = SELECT_RANDOMLY;
	params.t = SELECT_RANDOMLY;
	//params.s_size = 256;
	//params.t_size = 256;
	params.seed_len = 256;
	params.k = 8;
	params.prime_modulus = 1;

	return REPL_SUCCESS;
}

REPL_ERROR_T lcg_set_parameter(const char *key, const char *value) {
	int ival;

	ACCEPT_PARAMETER_pi(modulus_size_pow2);
	ACCEPT_PARAMETER_pi(prime_modulus);
	ACCEPT_PARAMETER_pi(s);
	ACCEPT_PARAMETER_pi(t);
	ACCEPT_PARAMETER_pi(seed_len);
	ACCEPT_PARAMETER_pi(k);

	return REPL_INVALID_KEY;
}
REPL_ERROR_T lcg_show_parameters(const char *experiment_name, FILE *msg_stream) {
	SHOW_PARAMETER_pi(modulus_size_pow2);
	SHOW_PARAMETER_pi(prime_modulus);	
	SHOW_PARAMETER_pi(s);
	SHOW_PARAMETER_pi(t);
	SHOW_PARAMETER_pi(seed_len);
	SHOW_PARAMETER_pi(k);

	return REPL_SUCCESS;
}

GEN_ERROR_T lcg_free() {
	mpz_clears(state, lcg_s, lcg_t, modulus, NULL);
	return GEN_SUCCESS;
}
