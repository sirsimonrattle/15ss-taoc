//
// Created by jan on 11/17/15.
//

#ifndef PRNGS_AES_OPENSSL_H
#define PRNGS_AES_OPENSSL_H
#include "generators.h"
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
DECLARE_GEN_REPL_FUNCTIONS(aes_openssl)

#endif //PRNGS_AES_OPENSSL_H
