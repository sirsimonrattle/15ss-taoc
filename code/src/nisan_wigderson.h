#ifndef NISAN_WIGDERSON_H
#define NISAN_WIGDERSON_H

#include "generators.h"

void next_poly(int *coeffs, int degree, int modulus);
int evaluate_poly(int *coeffs, int degree, int modulus, int x);
void construct_design(uint8_t *D, int s, int t);
DECLARE_GEN_REPL_FUNCTIONS(nw)

#endif
