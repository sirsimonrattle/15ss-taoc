#!/usr/bin/python3

## Change here ...


# Do not change anything below this line


from subprocess import call, check_output, Popen, PIPE, CalledProcessError
from os import path
from tempfile import NamedTemporaryFile
from datetime import datetime

try:
    from pony.orm import *
except ImportError:
    sys.exit("Pony not found. Install Pony: 'sudo pip3 install pony'")

db = Database()


class GeneratorRun (db.Entity):
    runid = PrimaryKey(int, auto=True)
    gen_name = Required(str)
    size_in_bytes = Required(int)
    runtime = Required(int)
    entropy = Required(str)
    machine_id = Required(str)
    time = Required(datetime, sql_default='CURRENT_TIMESTAMP')
    output_per_round = Optional(int)
    produced_per_round = Optional(int)
    exponent = Optional(int)


class FileGenerator():
    """
    Generate main_repl input file
    """
    def __init__(self):
        self.tmpfile = NamedTemporaryFile()

    def add_generator(self, parameters=None):
        """
        Add a generator to the parameter file. Takes a list of parameters where
        0 is the genererators name
        1 is the number of output bytes
        2 is the output bits per round OR None
        3 is the modulus' size OR None
        4 is the exponent OR None
        """
        self.tmpfile.write("set_name {}\n".format(parameters[0]).encode())
        self.tmpfile.write("use_generator {}\n".format(parameters[0]).encode())
        if parameters[2]:
            self.tmpfile.write("set_parameter {} {}\n".format(parameters[2][0], parameters[2][1]).encode())
        if parameters[3]:
            self.tmpfile.write("set_parameter {} {}\n".format(parameters[3][0], parameters[3][1]).encode())
        if parameters[4]:
            self.tmpfile.write("set_parameter {} {}\n".format(parameters[4][0], parameters[4][1]).encode())
        self.tmpfile.write("generate_bytes {}\n".format(parameters[1]).encode())

    def get_file_path(self):
        return self.tmpfile.name

    def rewind_file(self):
        self.tmpfile.seek(0)

    def close(self):
        self.tmpfile.close()


def get_mac():
    """
    Try several different names for network interfaces to determine unique MAC-ID for active machine
    :return: MAC-ID if found else String 'no_mac_found'
    """
    mac = get_mac_for_interface('wlp2s0')
    if mac:
        return mac
    mac = get_mac_for_interface('eth0')
    if mac:
        return mac
    mac = get_mac_for_interface('wlo1')
    if mac:
        return mac
    mac = get_mac_for_interface('eno1')
    if mac:
        return mac

    return 'no_mac_found'


def get_mac_for_interface(iface):
    """
    !! ONLY WORKS ON UNIX WITH IFCONFIG INSTALLED !!
    Calls ifconfig and parses output for given interface string
    Applies cases for german and english ifconfig output
    :param iface: String defining network interface
    :return: MAC-ID if found else None
    """
    try:
        out = check_output(['/sbin/ifconfig', iface], stderr=PIPE).decode('UTF-8')
    except CalledProcessError:
        return False

    out_words = out.split()
    if 'Adresse' in out_words:
        return out_words[out_words.index('Adresse') + 1]
    elif 'HWaddr' in out_words:
        return out_words[out_words.index('HWaddr') + 1]
    return False


def init():
    """
    Initializes Pony SQLite database for storage of generator runs
    """
    db.bind('sqlite', 'runtime.sqlite', create_db=True)
    db.generate_mapping(create_tables=True)
    if not path.exists('main_repl'):
        call(['make'])


@db_session
def write_run_to_database(entropy=None, time=None, parameters=None):
    """
    Take runtime parameters from generator list and entropy, time as parsed from output and store in database
    Timestamp and mac are automatically generated to later determine source of data
    :param entropy: byteEntropy of generated randomness
    :param time: Runtime of generator
    :param parameters: list with runtime parameters of generator
    """
    mac = get_mac()
    run = db.GeneratorRun(gen_name=parameters[0],
                          size_in_bytes=parameters[1],
                          runtime=time,
                          entropy=entropy,
                          machine_id=mac)
    if parameters[2]:
        run.output_per_round=parameters[2][1]
    if parameters[3]:
        run.produced_per_round = parameters[3][1]
    if parameters[4]:
        run.exponent=parameters[4][1]


def parse_output(lines=None):
    """
    Parse non cve-conform ./main_repl output regarding throughput and entropy
    :param lines: shell output of ./main_repl
    :return: entropy and runtime of generator in question
    """
    """
    :param lines:
    :return:
    """    
    line = lines[3].split(",")
    time = line[2]
    line = lines[6].split(",")
    entropy = line[2]
    return entropy, time


def run_generator(creator):
    """
    Runs ./main_repl with newly built input file and returns shell output
    :param creator: FileGenerator that created the input file
    :return: Shell output as taken from stdout
    """
    
    with Popen("./main_repl < {}".format(creator.get_file_path()), shell=True, stdout=PIPE) as po:
        out = po.stdout.read().decode()

    return out


def run_generator_and_store_in_db(parameters=None):
    """
    Wrapper for lower level add_generator, run_generator, parse_output
    and write_run_run_to_database functions
    :param parameters: The generator describing list with runtime parameters
    """
    print('[LOG] Running {}'.format(parameters.__str__()))
    creator = FileGenerator()

    creator.add_generator(parameters=parameters)
    creator.rewind_file()
    output = run_generator(creator)
    entropy, time = parse_output(output.splitlines())
    write_run_to_database(entropy=entropy, time=time, parameters=parameters)
    
    creator.close()


# ##### DO NOT CHANGE ABOVE HERE ##### #

"""
Basic lists containing runtime parameters for the generators
Structure: [generator name, bytes output, output bits per round, size of modulus, further parameters]
"""
LCGBase = ["lcg",512000,["k","2048"],["modulus_size_pow2","2048"], None]
LCGBaseFast = ["lcg",512000,["k","2048"],["modulus_size_pow2","2048"], ["s", 5], ["t",1], ["prime_modulus",0]]
RSABase = ["rsa",512000,["bits_per_round", "11"],["n", "256"],["e", "3"]]
AESnativeBase = ["aesni", 512000, None, None, None]
AEStextbookBase = ["aes", 512000, None, None, None]
AESopensslBase = ["aesopenssl", 512000, None, ["keysize", "128"], None]
BBS = ["bbs",512000,["bits_per_round", "3"],["n", "256"], None]


if __name__ == '__main__':
    init()
    """
    Feel free to do whatever you want from here on..
    """
    # Some Examples
    for i in range(1,800):
        RSABase[2][1] = "238"
        RSABase[4][1] = "3"
        run_generator_and_store_in_db(RSABase)
        RSABase[2][1] = "921"
        RSABase[4][1] = "65537"
        run_generator_and_store_in_db(RSABase)
        RSABase[2][1] = "11"
        RSABase[4][1] = "3"
        run_generator_and_store_in_db(RSABase)

        LCGBase[2][1] = "11"
        LCGBase[1] = 512000
        run_generator_and_store_in_db(LCGBase)

        LCGBaseFast[2][1] = "2048"
        LCGBaseFast[1] = 512000
        run_generator_and_store_in_db(LCGBaseFast)

        run_generator_and_store_in_db(AEStextbookBase)
        run_generator_and_store_in_db(AESnativeBase)

        run_generator_and_store_in_db(AESopensslBase)

        run_generator_and_store_in_db(BBS)
        print('Finished {}'.format(i))

